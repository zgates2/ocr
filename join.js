const fs = require("fs");
const path = "./txt"
const output = "combined.txt";

const files = fs.readdirSync(path);

const text = [];
for (let i=1; i< files.length + 1; i++) {
    const data = fs.readFileSync(`${path}/split_${i}.txt`);
    text.push(data);
    text.push(`--- Page ${i} ---`)
}

fs.writeFileSync(output, text.join("\n"));