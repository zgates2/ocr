35) — S. JOANNIS CHRYSOSTOM) ARCHIEP. CONSTANTINOP. 900
22). Quid hoc est? Post demonstratam absurditatem -- judicium conveniatis, id est, in condemnationem et in
lenior sequitur inerepatio , et jure quidem merito, — opprobrium. Non est enim cibus, inquit, neque men-
ne impudentiores evaderent. Priusquam enim rem — sa, quze cum pudore (ratris, cum despectu ecclesiz,
sbsurdam esse demonstraret, perfectam sententiam — et cum tauta voracitate νοὶ intemperantia conjuncta
extulit. dicens , Hoc awtem . pracipiens πο laudo — est. Hic non sunt Ixtiia, sed supplicium et paena,
(1. Cor. 13.17) Postquaim autem accurate demonstra- — Magnam enim vobis attrahitis ultionem dum fraires
viLillos esse complurium criminum reos, leniore uti- — contumelia afflcitis, ecelesiam contemnilis, sanctum-
tur criniuatone, in. jam dictorum apparatu et de- — que illum locum propriam domum facitis, quando ci-
monstratione accusationis vebementiam positam re- — bum seorsum sumitis. llis et νος auditis, fratres,
linquens. Deinde in mysticam mensam sermonem  Obturate ora eorum, qui apostolico dicto doctrinaque
transfert, ut eis plus timoris incuteret. Ego enim, in. — temere uiuntur; corripite eos, qui in suum aliorum-
quit, accepi a Domino, quod et tradidi vobis (lb, v. 25)., que damnum Scripturis utuniur. Nostis enim de qua-
Qualis lxec consequentia? De communi prandio lo- — nam rehoc dicat : Oportet autem ο lreses esse. in
queris, el horrenda mys[eria commemoras ? E[inm, Ν1Μ8, hneinpe de dissidio circa mensam oboriri solito,
inquit. Si enim tremenda illa mensa omnibus com- — quandoquideimm Alius quidem esurit , alius vero ebrius
munis proponitur, el diviti et. pauperi, neque illa — est (1. Cor. 11. 21).
fruitor uberius dives, et parcius pauper, sed unus ho- Adhoriatio ad pauperum cwram. — Ac cum recia
nOr, el unus accessus; ac donec omnes communica- — flde vitze rationem cum dogmatibus congruentem ex-
verint, et participes fuerint hujus spiritualis et sacr:e — hibeamus , multam erga pauperes benevolentiam ,
Nens:e, α proposita sunt non retrabuntur, sed — multam egenorum curam suscipiamus ; negotiatio-
stant sacerdotes omnes, vel emnium pauperrimum et — nom spiritualem exerceamus, nihil plus inquiramus,
vilissimum exspectautes : multo magis in hac sensili — quom necessitas postulet. Hx» sunt divitize, lxec ne-
mensa id agendum. jdeo illam dominicam cgenam — gotiatio, bic thesaurus indeficiens, si omnia nostra
memoravi. Ego enim accepi a Domino, quod et tradidi — in cxlum iransferamus , ο circa depositi custodiam
vobis, quogiam Dominus Jesus, in qua nocte tradebatur, — meiu soluti fidamus. Duplex namque ex eleemosyna
accepit panem, el gratias agens . [regit, ε dixit ; Πος — nobis luerum accedet : quod scilicet non ultra timea-
est corpus meum , quod pro multis frangitur, in remis- — mus circa depusitas pecunias, πο fures aut muroruin
sionem peccatorum. Πος fucite in. meam commemora- — elfossores, aut. nequissimi servi ipsas pessumdent :
Lionem. Similiter et calicem postquam cenavit, dicens : — e& quod sic deposit:e non temere defoss: sine fructu
ΙΠς caliz novum testamenium. ν in meo. sanguine — jaceant; sed quemadmodum radix in pingui solo
(Jbid. v. 83 - 25). plantata annuos tempestivosque frucius profert, sic
B. Deiude multa loquuius circa eos, qui indigne — et argentum in pauperum mauibus insitum, non an-
miysteria participant, ubi λος vehementer tetigit et — nuos solum, sed etiam quotidianos spirituales nolis
coarguit, docuitque eos, qui sanguinem et corpus  referat frucius, fiduciam erga.Deum, peccatorum ve-
Christi temere ac perfunctorie accipiunt, eamdem — niam, cum angelis propinquitlatem, conscientiam bo-
subituros esse poenam, quam ii qui Christum occide- — nam, ketitiam spiritualis exsultationis, spem pudore
runt; rursus Δά propositum argumentum sermonem — vacuam, mirabilia bona, qux prwparavit Deus ti-
refert, dicens : ltaque fratres, cum convenitis ad man» — mentibus se, iisque qui ferventi ardentique animno
ducandum, invicem exspectate. Si quis autem esurit, ἀρ — requirunt. misericordiam adventus ejus. Quam πος
. manducet, ut non in judicium conveniatis(1. Cor. 11. 33, — omnes,, bac vita secundum placitum ejus transacta,
34). Vide quomodo latenter etiam voracitatem eorum — assequi contingat, nec non zeternum eorum qui salu-
damnet. Non dixit, Si esuritis, sed, Si quís esurit, ut — tem consequuntur, gaudium, gratia el iiserationibus
quisque metu et pudore ductus, ne reus hoc in nego- — veri Dei et Salvatoris nostri Jesu Christi, cui gloria
iio videalur, sese emendare przeoccupet. Atque a — etimperium cum Patre ejusque sanctissimo Spiritu
metu supplicii sermonem concludit dicens : Ut non in. in s:ecula seculorum. Amen.
- EE BN
MONITUM
Soannes Chrysostemus, qui non semel ex tempore concionatus est, hanc habuit homiliam occasione.
mendicorum et egenorum, quos biemis tempore jacentes, eum per forum et angiportus ad ecclesiam per-
geret, repererat. Hunc ad concionandum apparatum, dum iter faceret, adornavit : el lianc. elegantem
€oncionem de eleemozyna αὐτοσχεδιαστὴς orator pronuntiavit. Eam porro Antiochiz habitam fuisse in-
dicst, non modo cum ait, se per forum οἱ angiportus, διὰ τῆς ἀγορᾶς xal τῶν στενωπῶν, ecclesiam pe-
Miisse : reque cnim cum episcopus Constantinopolitanus erat, tam proeul ccclesia degebat; sed etiam οἳ
