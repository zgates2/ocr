29 "— |N ILLUD, PATER, SI POSSIBILE EST, TRANSEAT, ETC. — MONITUM. 50
inquit, cjieiatur; οἱ ἀυο infidelis sit, πο expella- — dealienis delictis collectam facillime poterimus sum-
tur : si, inquit, in te peccaverit, absciude; si 11 me, — movere : sí enim meminisse oportel delictorum, pro-
detineas. Πα. et hic, cum tanta peceata adversum se — priorum tantum meminisse oportet ; quod si proprio
rommissa erant, veniam dedit, cum autem adversus — rum rmemores fuerimus, numquam aliena delicta
to:servum , lice minora paucioraque, nequaquam — suppulabimus : contra si illorum obliviscamur, ist a
remisit, sed ultionem persequutus est : et hic quidem — facilius obversabuntlur nostris cogitationibus. Nam et
tum scelestum appellavit, illic ne verbo quidem con- — hicsi decies mille talentorum memor fuisset , num-
tistavit. Et ideo Πο illud quoque adjunctum est, —quam centum denariorum recordationem retinuisset,
quod dominus iratus tradiderit eum tortoribus : eum — 1deo postquam illorum oblitus erat , factum ost , ut
autem ab ]ο reposcebat rationem decies mille talen- — conservum suffocaret, et pauca volens exigere, neque
i , nibil istiusmodi additum fuit : ut intelligas illic — illa. quidem assequutus esi; scd el decies mille
quidem calculum subductum fuisse, non irarum, sed — talentorum molem in suum ipsius caput retraxit.
sollicitudinis, quze venie locum pararet. lloc autem — Quocirca audacter dixero , omni peccato istud gra-
peecatum dominum plane exacerbavit. Quid igitur est — vius esse : quinimo non ego lhoc dico, sed Christus
pejus iujuriz recordatione, cum beniguitatem Dei jam — per parabolam istam istud declaravit, Si enim. non
depromptam revocet, et qux: non potuerunt efücere — decem millibus talentum, hoc est , ineffabilibus pec-
alia peccata , hzc efüciat ira adversus proximum ? — catis istud gravius esset, nequaquam hujus causa illa
Atque scriptum est, Dei dona esse sine poenitentia — omnia jam donata revocala fuissent. Nibil igitur ita
(Hom. 11. 99). Cur igüuur Μο post eductum munus, — studeamus, quam ut ipsi ira vacemus, et nobis in-
post progressam benignituem , reduetus.est caleulus ? — fensos reconciliemus : cum illud sciamus, nullam
Propter injurie memoriam : quamobrem non erra- — 100 oralionem, nec eleemosynam, nec jejunium, nec
verit quis, οἳ hoc peecatum omni peccato gravius — participationem mysteriorum , aut aliud quidpiam, si
pronuntiet : alia enim omnia veniam impetrare po- — memores offensarum simus, nobis patrocinari posse
tuerunt, istud solum adeo non potuit veniam adi-  in illa die : ut contra si hoc peccatum superemus,
pisci, ut jam deleta, et jam exstincía iterum reno-  etiamsi mille delictis iuquinati simus , veniam ali-
varit. : quam consequi πος posse. Νοο mcus sermo est iste ,
Nihil Ha Deus odit ut mentem memorem injurig. — —— sed illius qui tunc nos judicaturus est. Dei. Quemad-

Duplex igitur malum pertinax injuriz: memoria, et — modum boc in loco pronuutiavit : 4ta , inquiens , fa-
quod nullam excusationem apud Deum habeat, et — ciet Pater meus, si. non. remiseritis unusquisque ex cor-
quod reliqua peccata jam remissa ijerum revocet, et — dibus. vestris. Et rursum  alibi : Si. remiseritis homi-
contra nos constituat.Quod ipsum et hie factum est : — nibus delicta sua , remittet et. vobis Pater vester catlestie
nibil enim, nibil omnino ita Deus odit, eLaversatur, — (Matth, 18. 25 et 6. 14). Ut igitur et bic mansuetam
ut hominem offensarum memorem, et tenacis irz. Et — et mitem agamus vilam, et illic veniam et remissio-
illud quidem przcipue ox hoc loco declaravit, et ex — nem obtineamus : studendum οο satagendum est , ut
oratione , ia qua3 jussit nos ita dicere ; Dimilte nobis — quotquot babemus Inimicos, οος nobis reconciliemus :
debita nostra , sicul et πος dimittimus debizoribus nostris — ila enim et Dominum nostrum, etiamsi sexcenties pec-
(Matth. 6. 12). llzec. igitur omnia docti, et parabola — caverimus, nobis reconciliabimus , et futura. bona
$sta in cordibus inscripta, cum cogitaverimus qux — adipiscemur, quibus nos dignos opto fleri, gratia et
passi sumus a eonservis, cogitemus qu:e fecimus in — lumanitate Domini nostri Jesu Christi, cui gloria et
ipsum Dominum, et metu propriorum crimiuum iram — imperium in sxcula scculorum. Amen.

ln EU —— A—A————ÓMÀ————————————————ÁÓÓáá -ς

(Ν ILLUD, PATER, 51 POSSIBILE EST, TRANSEAT Α ΜΕ CALIX ISTE, Erc.
bus supplantare. Unde liquet hareticos illos, qui Filium Patre minorem , ipsique dissimilem dicebant,
nempe Anomoos, Chrysostomi concionibus interfuisse. Πο autem frequenter Antiochim accidisse, et
praesentes hzreticos Chrysostomum confutasse, ipse diserte ait, Tomo I de Incomprehensibili contra. Au-
om«os p. 450, idipsumque postea subindicat p. 525, et p. 509, in hzc verba queis Christus Petrum regu-
lit, Vade post me, Satana, etc., eadem pene ipsa quae liíc paulo post initium habes, iisdemque fere verbis
«ommemorat. Quamobrem putaverim hanc homiliam Antiochiz habitam fuisse, nec diu post illas con-
ἵγα Ánomoos llomilias ; quod tamen conjecturz tantum loco dictum sit.
Interpretatio Latina est Frontonis Duczi.
