339 QUALES DUCENDAZE SINT UXORES. l. 230
frustrabitur, quod propter Dei timorem tantum moile- Parentibus preferenda uxor. — (uomodo magnum
stiarum sustinueris, et morosam feminam zquo animo — est, dic mihi ? Quod virgo asservata omni tempore ,
tuleris, οἳ membrum tuum constanter pertuleris. Est — sponsum numquam ante visum mox a prima die sic
enim uxor membrum nostrum necessarium; et hoc — desiderat , et amat tamquam corpus proprium : rur-
potissimum nomine debetur ei benevolentia. Quod οἳ — sum vir quam numquam vidit , numquam alloquutus
Paulus his verbis pracipit: Sic debent viri suas uxores — eSt, mox a prima die przefert czeteris omnibus, et
diligere sicut. propria corpora. Nemo enim umquam — amicis et familiaribus , denique ipsis parentibus. Pa-
suam carnem odio habuit, sed nutrit el fovet illam,sicut — l'entes item , si per aliam causam auferatur eis pecu-
εΙ ε|||'|΄'*ι|ι| Ε΄ες[ς||΄ι|',| ; |'||,'||΄ι||,| membra sumus gju;- nia , dolenter ferunt , θὲ in jllS trahunt *eum qui abs-
dem corporis, ex carne ejus et ossibusejus(Ep.5.28-50). — tulit : homini autem sape numquam ante viso et
Ecclesia cum Eva collata. — |Magnum lic subindi- — ignoto etiam dotem una cum filia luculentam in ma-
cavit mysterium , dicens, Ex caruibus ejus εἰ ossibus — nus dant. 1dque libenter faciunt, neque se damno
ejus (a).] Sicut enim Eva, inquit, ex latere Adami — affici existimant ; sed videntes abduci filiam non me-
prodiit, ita et nos e Christi latere. Πος enim est, Ez — minerunt. consuetudinis , non dolent, non auguntur ,
carne ejus et ossibus ejus (Gen. 2. 21). Sed Evam qui- — sed gratias agunt insuper , et rem . optabilem putant
dem ο latere Adami prognatam scimus omues, et di- — filiam e domo cum multa abduci pecunia. αοο igitur
serte hoc in Βοτίριυτα legitur, quod immiserit ineum — omnia Paulus considerans, quod parentibus relictis
soporem, et exempta una e costis ejus :edilicaverit — atmbo sibi mutuis jungantur nexibus, novumque con-
mulierem : quod autem Ecclesia e Christi latere orta — sortium majorem antiqua consuetudine vim accipiat :
sit, undenam discere possumus? Ποο quoque Scri- —animadverteusque non esse hoc humanum negotium,
piura indicat. Postquam enim Christus in crucem — sed divinitus amorcs tales inseri, ut nuptze pari et
sublatus et affixus exspiravit, Accedens unus militum — tradentium et accipientium cum Letitia elocentur at-
pupugit ἰαίκε illius, et exiit mde sanguis et aqua (Joan. — que assumantor, Aysterium, inquit, hoc magnum
49. 34) : et ex illa aqua et sanguine tota constat Ec- — es!. Et sicul receus natus infantulus adspectu statim
vlesia. Testis est ipse dicens : Nisi quis renatus fwerit — parentes agnoscit, priusquam sciat. voces edere : ita
ez aqua et spiritu, nou potest introire in regnum calo- — Sane et sponsus ac sponsa, nemine sequestro, nemine
rum. (Joan. 5. ὃ } : sanguinem autem appellat spiri- — hortatere, primo statim. adspectu conciliantur invi-
tum. Et nascimur quidem per aquam baptismatis, — cem. ld cum in Christo etiam auimadvertisset, et
alimur autem per sanguinem. Vides quomodo simus — przsertim in Eeclesia, non sine stupore quodam ad-
ex carne ejus, et ex ossibus ejus, dum ex sanguine — miratus est. Quomodo igitur in Christo et Ecclesia
illo ο aqua tutn nascimur , tum alimur? Et queinad- — idem contigit ? Sicut spousus relicto patre ad sponsam
modum Adamo dormiente mulier est condita, sic ChrE — properat, ita et Clristus relicto pateruo solio, veuit
sto moriuo facta est Ecclesia ex ejus latere, Nec ob — 3d sponsam : non nos evocavil ad supera, sed ipse
hic tantum. amanda est uxor, quod membrum no- — ultro ad nos advenit. Cum autem adventum . audis,
strunr est, el ex nobis originem habuit, sed quia — non migrationem, sed atlemperatonem intellige :
legem etiain super hoc Deus statuit, sic diceus, Pro- — nam etiam cum nobiscum esset, cum Patre erat.
pterea relinquet liomo patrem. suum et mnatrem , et αἷ- ' Quapropter inquit : Mysterium Λος est. magnum. Ma-
herebit uxori sue , et erunt duo in carne una ( Gen. 2.— gnum sane eliam apud homines; sed cum video in
94). Nam ideo nobis hanc legem Paulus legit, u imodis — Christum quoque οἱ Ecclesiam idem competere, tum
omnibus nos ad hunc amoretn compelleret. Πς mihi — certe iniraculo rei reddor attonitus. Quamobrem cum
considera sapientiam apostolicam : non enim selis — dixisset, Mysterium hoc. ntagnum est , subjecit mox :
divinis aut solis humanis legibus πος inducitad aman- — Ego autem dico in Christo et in Ecclesia (Ephes. 5. 32).
das conjuges, sed mixtim utrisque : ut sublimes qui- — 1taque cum scias quantum sit in conjugio mysterium,
dem viri ae philosophici πιασῖς moveantur ezelestibus, — et quanti figura negotii, non temere de hoc delibera,
infirmos autem Ο humiles naturale amoris incita- — neque ducturus sponsam, pecuniarum accessionem re-
mentum magis niovcat.. ldeo primum a Christi hene- — spice. Non enim negotiatio, sed vite societas conju-
ficiis doctriuam hane incipit sic dicens, Diligite uxo- — gium existimandumn cst.
res vesiras , sicut et Christus dilexit Ecclesiam ( Ephes. &. Mulier creata ut auzilietur viro. — Multos enim
5. 95); deinde rursus idem agit human's rationi- — audivi dicentes , ]ς opulentior post nuptias factus
bus : Sic debeni viri suas uxores diligere, ut s«a ipso- — egt, cum fuisset pauper antea ; nunc ducta uxore di-
rum corpora (Ephes. 5. 28), deinde rursum ad Chri- — vite et ipse agit in deliciis. Quid ais, bone vir * ex
stum revertitur ; Quoniam membra sumus ex corporé — uxorc lucrari cupis, et non. erubescis ? nou pra pu-
ejus, et ex carne ejus, ct ex ejus ossibus (1b. v. 50); ac — dore sub terram te abdis talium lucrorum appeteus ?
rursum ad lumanas rationes redit, Propterea. relin-.— |yeccine sunt verba viri ? Mulieris unum est oflicium ,
quet homo patrem situm et matrem, et adlzrebit wxori — yt parta custodiat, ul conservet reditus , el curet rem
sua (15. v. 51); lcctaque hae lege : Mysterium, inquil, — domesticam : nam idcirco eam Deus dedit, ut hac 1
Λος magnum est ( Ι91ά. v. 52). te et in czeteris nobis sit auxilio. Quoniam enim vita
(α) tiiec, qux unciuis clauduutur, πο sunt iu Graco, sed l.ec nostra e duobus constat , e rebus privatis ct pu-
Ra legit luterpres.
