155 8. JOANNIS CHRYSOSTOMI ARCHIEP. CONSTANTINOP. 156
Quod utique Paulus quoque fecit : cum enim justo- — ramus etiam et compungamus : conteritpr autem,
rum omnium primus esset, primum se esse dicebat — dum suorum perpetuo peccaterum recordatur. Si ita
gecceatorum (1. Tím. 4. 1$) ; neque dicebat solum, — éllam humiliaverimus, ne &i velit quidem in super-
sed et credebat, cum a. przceptore didicisset, post- — biam erigi poterit, dura a conscientia tamquam freno
quam emnia fecerimus, debere nos servos inutiles ne$ — quodam correpta, ue sc erigat, reprimetur, et ad
vocare (Luc. 17. 10). ]stud est humilitas, istum imi- — modestiam servandam in emuibus compelleiur. lia
temini qui recte factis insignes estis, publicanum — poterimus οἱ apud ï)eum graliam invenire : Quanto
wero vos qui peccatorum pleni esüig: et confiteamur — enim miajor es, inquit, tanto magis teipsum humilia,
quod eumus, perculiamus pectus, et menli nosira — «f insenies gratiam apud Deum (Eccli. 3. 20). Porro
persuadeamus, ut nihil magaum de se concipiat. Si — qui gratiam apud Deum invenerit, nihil sentiet mo-
enim ita fuerimus affecti, sufficit hoc nobis ad obla- — Je;tiz, sed et in hac vita poterit cum Dei gratia cune
Aionem et eacrificium : sicut οἱ David dicebat : Sacri- — cia illa adversa facile pertransire, ac repositas in
Jicium Deo spiritus coniribulatus ; cor oentritum et lu- — altera vita peccatoribus pesnas eíffugere, przcedento :
miliatum Deus non despiciet (Psal. 50. 19). Non sin- — ubique gratia Dei, et omnia illi propitia et placida
pliciter dixit. Aumiliatum , sed et contritum; quod — reddonte : quam utinam omnes ol4inere possimus in
enim est contritum, coofractum est: neque ebim — Christo Jesu Domino nmostre, per quem et cum quo
potest, quantumvis cupiat, efferri. lia nos quoque  Patri gloria, una. cum sancto Spiritu, nunc et sem-
&ion hbomiliemus tantum animam nostram, sed conte- — per, et in s:ecula &culorum. Amen.
MONITUM .

Nullam hsc concio prz se fert temporis netam, qua vel annus deprehendi vel internosci valeat Antio-
chiene an Coustantinopoli habita fuerit, Czterum omnes styli note bic Chrysostomum auctorem desi-
guant ; ut mirum sit virum dectum et accuratum Hermantium eam in νοθείας suspicionem vocare non
dubitavisse, quem ideo jure refellit Tillemontius, et styli proprietate permotus οἱ Catalogi Augustani per-

IN DICTUM ILLUD APOSTOLI, NON SOLUM AUTEM, SED ET GLORIAMUR IN TBIBULATIONIBUS, SCIEN—
TES QUOD TRIBULATIO PATIENTIAM OPERATUR ( Hom. 5. 2), kr nELIQUA (α).
-D WE -

1. Labores pro virtute subeundi. — Laboriosum est — Φοϊνοτιαπάϊ, atque ad labores pro virtute subeundos

quidem agricol:e boves jungere, vomerem trabere, — incitandi. Nam si propter spem futurorum, idque ut
sulcum ducere, semina spargere, hyememferre, fri- — eorum aliquis ea bona eonsequi possit, qu:» cum hac
gus tolerare, fossam circumducere, ingruentem aqua- — vita finiuntur, hzc illi sustinent, multo magis vos
rum copiam a satis arcere, fluminum ripas alüiores — 0porlet im audiendo perseverare, ut spirituali do-
reddere, ac per medium agrum profundiores sulcog — Clrina imbuamini, ac pto vita zeterna. puguam genee
ducere : sed μο Operosa, quzeque multo labore il- — 0se sudorcsqne tolerare. Nam illorum quldem in-
lum fatigant, levia fiunt ac facilia, dum spe concipit — Certa spes est ac temporaria, et szpe dum sola ex-
segetem vermantem agricola, falcem exacutam, ac — spectatione laetantur, vitam flniunt, spe quidem so
repleam manipulis aream et fruges maturas, qua — blectantes, sed rebus ipsis uecdum appropinquantes,
multa cum σ domum deferuntur. Sic et guber- — tametsi graves itlas difficultates exantlarint. Exermpli
nator szevis audacter se fluctibus commiltit, tempe- — €aus2, post multos illos labores ac sudores agricolà
slatem et fureus pelagus szepe contemnit, ventos iua- — nonpumquam, dum falcem exacuit, οἱ ad messem se
stabiles, marinas procellas, etlongas peregrinationes — Comparat, vel immissa zrugine, vel locustarum mul-
ferre novit, cum sareinas mercium secum reputatet — titudine; vel immodica pluvía incidente, velalia qua-
" egotiationis portus, οἳ immensas opes inde compa- — Diam importata calamiitate ex aeris intemperie, vacuis
r audas intuelur. Νοο aliter miles fert vuluera, jaculo- — manibusdomum revertitur, postquam labores quidem
rum nubemexcipit, famem tolerat, frigus, longas pe- — $ustinuit, sed speratis frugibus excidit. Puri ratione
regrinationes, e& iu acie subeunda periculo, dum — gubernator dum mercium copla Letatur, expausis
tropma, victorias, et coronas secnm reputat, qu:e — magna cum voluptate veli$ eum multa maria fuerit
inde pariuntur. Ad' quid autem ο commemoravi- — €mensus, in ipso persaepe portus ingressu vel occur-
mus, 2ut quid lec exempla sibi volunt? Hinc nimi- — renterupe vel ad saxum in mari latens aut ad scopu-
rum occasionem arripere placuit νος ad audiendum — Jurn aliquem allisa navi, ac inopinato ejusmodi casu
(a ^ollata cum Mss. negio 2315, et Colbertino ἐ9. "— perculsus, cunctis mercibus amissis, post infinita illa
