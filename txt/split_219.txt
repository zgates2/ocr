235 QUALES DUCENDJE SINT UXORES. l. 236
et honestate crterisque virtutibus agamus, ut et  quamur, gratia et bemignitate Domini nostri Jesu
Deum misericordem habentes regnum czlorum asse- — Christi, cui sit gloria in szecula szculorum. Atnen.
LAUS MAXIMI,
, ET QUALES DUCENDAE SINT UXORES (α).

4. Maximus Chrysostomi collega laudatur. — Quod — stra verba vehementer tangant auditorum ammos.
quidem proxim:e collecte interesse mihi non conti- Justificationis pars  peccatum — damnare. . Mature
gerit, dolui: quod vero ipsi tum magis opiparam ς muptiis deliberandum. --- Satius quidem esset
mensam nacti sitis, gavisus sum. Meus enim in ἰγᾶ-  ommino a peccando abslinere, non parvum tamen
liendo jugo compar , nuper proscisso sulco ubere lin- — ad salutem momentum est, agnitum peccatum dam-
gua inspersitetiam seinina, omnique diligentia vestras — nare, et castigare diligenter conscientiam , utpote
excoluit anünas. Vidistis liuguæ purimlcln , audistis cum hoe ipsmn jus[i(ica[ionig pars si[' et .mpo-
bene tornatam orationem, datum est vobis aqua frui — sterum a peccando arceal. Hine est quod etiami
saliente. in vitam zternam : vidistis fontem scaten- — Paulus eos, qui peccaverant , contristatog videus
tem fluviis aurum purum deferentibus. Ferunt quem- — gaudebat, non ob eorum tristitiam , sed quod per
dam fluvium auri ramenta inferre accolis. non quod — lanc occasionem correcti essent. Gaudeo, inquit,
ejus aqua naturam auriferam babeat, sed quia fonteS — non quod contristati fueritis, sed quod contristati sitis
ejus oriuntur in montibus metallieis : per eos labens — ad penitentiam : nam qui secundum Deum est. dolor ,
fluvius arenasque trahens aureas, accolentibus the- — is ponitentiam ad. salutem non dubiam parit. (9. Cor.
sauros affert, ultro oblatis divitiis. Hujus fluminis in — 7, 9. 10). Ergo sive propriis, sive alienis peccatis
morem nuper hic magister labendo per Sceripturas — tunc indoluistis, ingenti laude estis dignissimi. Nam
tamquam montes metallicos; quovis auro pretiosiores — et qui proalienis dolet, apostolica prz δο fert viscera,
sententias intulit vestris animis. Quapropter scio fo- — ct beatum illum imitatur, dicentem, Quis infirmaiur,
re uL apparatus noster hodie vobis videalur exilior. — et ego non infirmor? quis scandalizatur ,. et ego mon
Qui enim paupere mensa continue fruitur, si quando — uror(2. Cor. 11. 29)? Et qui propriis niordetur ,
forie fortuna in copiosiorem incideri, mox ad prO- — debitam sceleribus suis penam exstinxit, et per hunc
príam reversus tanto magis sentit ejus inopiam. Át- — dolorem in futurum se cautiorem reddidit. Πος de
tamen nihilo segnius oralionem aggrediar. Nostis — cauga ego quoque cum viderem νος demisso capite ,
enim a Paulo docti οἱ saturari, οἱ csurire, et abundare, gementesque et tundentes faciem, gavisus sum cogi-
et egere, mirari divites, nec tameu pauperes COntéDi- — (ans quantus sit fructus ejus tristitie. Quo [it ut et
nere (Philipp. 4. 12). Et quemadmoduim vini amanteS — jodio idem argumentum tractare libeat, utquibur
et potatoresdeleetantur quidem generoso, sed deteriu$ — cordi sunt nuptize, mature de hoc negotio deliberent
non fastidiunt : sic vos quoque qui deperitis dività — Si enim domum empturi aut mancipia, curiose consi-
eloquia, nagistros doctiores suscipitis, ita tamen Ul — deramus tum venditores, tum priores dominos, ipso-
nom vulgare studium impendatis eliom medioeribus. — cum, quoque venalium tam corporis habitudinem ,
Qui enim luxu diffluunt, vel ad sumptuosam mensam — quan indolem animi : quanto magis dispicienduo est
uauseabundi discumbunt : contra sobrii qui esuriunt — o futura conjuge ? Domum enim, si vitiosa sit, licet
ac sitiunt justitiam, vel ad pauperem mensam !n38U2 — qenuo vendere, sicut et servum nequam compertum
alacritate accurrunt. Quod autem hzc non loquar ad vendilori restiluere : uxorem vero semel accepiam
gratiam, proximo, quem nuper ad vos habui (5), — pop itom fas esta quibus acceperis reddere : sed ne-
sermone satisapparuit. Cum enim multa verba face- cesse est in perpetuum eam domi habere, nisi malis
remus de conjugio, docentes meruimn esse adulterium — . t improba ο]ροία reus adulterii juxta legem divi-
dimillere uxores, aut dimissas vivo etiamnum prio- — i Geri. Quando igitur uxorem duciurus es, non
re marito ducere, allegaremusque legem ab ipSó — solur (ivilo jus, verum etiam ecclesiasticum legito :
Christo latam, ubi diserte legitur, Qui dimissam ducit, nam secunduimn boc, non illod, extrema die judicandus
conmittit adulierium, et qui praier stupri causam di- — 4 peo eg : et illo contempto szpenunero pecuniis
mittit uxorem, facit eam adulteram (Mauh, 5.32), anin-.— um mulctaberis, hoc autem calcato i animz sup-
adverti imultos demisso capite c:edere sibi faciem, plicium incides οἳ ignem inexstinguibilem.
ac ne vultum quidem posse attoHere : ad quod. spe- 4, Tu vero uxorem ducturus externi juris peritoi
Ctaculum sublatis in czelut oculis, Denedictus , dixi, diligenter consulis, et illos frequentans sollicite dis «
Dcus, quod nun mortuis inclamemus auribus, sed no- quiris, quid futurum sil, οἱ ea decedat nullis relictis

(a) Collata cum Mss. Cofbert. 970 et 4030, in gluiþus deest libe::is, quid sï β|ίυω relinquat, Ή|ίά μ'ε… si duo π'ε;νΐ
illud, Luus Mazimi, perinde avque in Edit. Savil. : αἰΜαίπρη — fuerint superslites , tum quomodo vivo ραίτε suo e
.e..uinumleææ; ällsdä'n :::i Πι;ο…ιο. Admodun) pauca hic functove γος sui$ usura sit ,.quanlum- cx. ejus patri-
m:(':;'æ:[:, nomil. dv libello repudii, nionio marito, quantum fratribus mulieris obvenire
