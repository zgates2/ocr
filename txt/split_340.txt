241 Β. JOANNIS CIIRYSOSTOMI ARCHIEP. ΟΟΝΞΤΑΝΤΙΝΟΡ, Σ
tiz participes fuimus, οἳ divinis exculti dogmatibus, — vilemus, omnem misericordiam in egenos exhibea-
qui de rebus futaris possnmus philosoplisri, neque — mus, ut et πος assequi. bona futura mereamur grati
famem videmus instantem, et qui multo plura, quam — et benignitate Domini nostri Jesu Christi, cum que
mulier possidemus, quamnam poterimus excusatio- — gloria Patri, una cum Spiritu sancto in szcula saca-
nem obtendere, αυὶ facultatibue parcimus, et no-tr»— Joru:m, Amep.
saluiis prodigi samus ? Ut igitur gravia Hls tormenia
e
ΞΞΞΞΞΠΓΠΠΥΥΙΤΙΙΤΙΤΙΩΣΙΥΤΥΥΥνΥΣΗΩ:ὴἸ.ὴ.......ὺὰὰ) ΞΞΞΞΣΡΡπ π σµΐὙὐλππἵπτΟΑΎὙΥΥΝὙπἉΎλΟΎπὙὍὍΤΛΗΥίΤµὙΎππΤΗµΗΗΗµΡµὙπΥὍΕΙΙΗΑ Αἳ-ἳἲ-ἳ--“-ἳ-.Σὗ
MONITUM

Hanc pulcherrimam concionem habuit Clirysostomus Antiochie, ut arguilur ex ς α sub initium
dicuntur, nempe α hunc martyrum locum populum ventitasse. Extra urbem nempe erat ecclesia, proptet Ι
mariyruim reliquias (requentata, quo pergebant populi pietatis gratia, et ubi conciones frequenter habe-

DE FUTURORUM DELICIIS,
ET PRAESENTIUM YILITATE (a).
-..-..οΘο....-.

4. Auditoribus de alacritate. gratulatur ; que vera — hominibus quidem imperct, sed αἴοριθας animi ser-
sint insignia magistratus exponit. —Vehemens msius — viat, hunc ego dixerim plus quam omnes homines
est, et molestus ardor ; auiamen alacritatem vestram — esse servituli obnoxium. Et quemadmodum qui fe-
non debilitavit, πος audiendi cupiditatem repressi. — brím liabeat visceribus ας venis intus inclusam, ta-
"Talis est auditer fervens et attentus; audiendi desi- — metsi pihil tale specics corporis externa prze se ferzt, '
derio corroboratus omnia. patienter ferre potest, ut — omnino febre maxime correptum asserent medici, li-
cupiditatem hanc prrcelarim et spiritualem expleat, — cet ignorent imperiti : sic et ego illum cui serva sit '
et nec a frigore, ncc ab zstu, non a negotiorum — anima et animi affectibus mancipata, licet nihil tale
turba, non a multitudine curarum, nonab aliis rehus — facies externa, sed contrarium prx se ferat, servituti
ullis ejusmodi potest supplantari : quemadmodum et — obnoxium prz czeteris dixerim, utpote qui vitiorum
supinum ac desidem non aeris bona temperies, nan — febrim grassantem intus hiabeat, ας tyrannidem pas- W
otium οἳ securitas, non facilitas aut voluptas potest — Sionum ipsi animze. iusidentem : magistratum aufepa
excitare, sed correptus somno quodam residet cri- — gerere, et liberum esse ac regibus ipsis augustiorem,
mine ac viluperatione dignissimo. Vos autem tales — quamvis pannis indutus sit, quamvis iu carcere ha- ;
non estis : sed iis qui nostram incolunt urbem multo — bifct, quamvis catena sif cireumdatps, eum qui lijus i
meliores estis, Siquidem νος urbis caput ac verteg — tyrannidis jugum cxcusserit, et neque pravis cupidi-
estis adeo erecti αο vigiles, οἱ qui ea qux» dicuntur, — tatibus teneatur, nec absurdo paupertatis et jgnomi-
perpetuo persequi studiose soletis. Hoc mibi theatrum — niz, neque eorum qux in hac vita molesta videntur,
regum aulis est augustius. Nam illic quidem οο quo» — timore correptus contremiscat. !
tribuuntur, una. cum hac vita finiuntur, et tumultus 9. Ejusmodi magistratus non pecunia venales pro-
plena sunt ac. turbis redundant : μς vero nihil t31e; — staut, neque invidorum pateul incursibus : hunc ac-
sed et securitas omnis, et honor a turbis immunis est, — cusatoris lingua non novit, nec oculus invidorum ,
et magístratus qui numquam finiuntur, πος ipsa morle — neque insidiatorum machinz, sed tamquam in invios
interrumpuntur, sed tum temporis tutieres fiunt. Nolo — labili quodam philosophiz domicilio residens perma-
enim mihi commemores eum qui sedcat in curru, su- — net semper inyictys, neque tantum czleris perum |
percilia attollat, multoque sit satellitio cinctus, neque — adversarum (αδῖνης, sed nec ipsi morti cedif.
eingulum et vocem przconis : ποῖο mili magistratum BMlartyrum potestas quanta ; spirituales divitie distri- |
Índe designes : sed ab animi statu, sl affecjibus im- — butione crescunt ; cur caducas esse res vitae hujus. vo-
peret, si vitia subigat, videlicet, si divitiarum cupi- — (uerit Deus ; angelica est vita evangelica. — Ostendunt :
ditati dominetur, si corpormn inexplebilem amorem — hoc martyres, quorum corpora quidem dissoluta suut,
suhegerit, si non invidia tabescat, si non inanis glorie — et jn pulverem cineremque redacta : magistratus au- ΐ
perturbatione distraliatur, οἱ egestatem non metuat — tem ac principatus quotidie vivit, et operatur , dum
εἰ tremat, si πο in deterius mutationem, si hoc ti-— damones abigit, et morbos in fugam convertit, ἀπ
more non exanimetur. Talem mihi magistratum os- — iutegras urbes excitat, et populos in hunc loeum de-
tende : hoc. euim est gerere magistratum. Quod οἱ — ducit. Tanta magistratus et. imperii istius est virfus,

(μ) Collata cum uss. neg. 1974, et Colb. 5058. non vivorum tantum principum, scd etinm vjla fua-
