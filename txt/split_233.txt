259 QUALES DUCENDAE SINT UXORES. Il. 240
Jayen τῷ υἱῷ µου γυναῖκα ἀπὸ τῶν θυγατέρων — οὖσα, ἀλλ᾽ ob χειροποίητος, οὔτε ἐπιτετηδευμένη. Δεὰ
τῶν Χαναναίων, dAX εἰς τὸν οἴκον τοῦ πατρός τοῦτο καὶ ὑγείας ἀπέλαυον καθαρᾶς, καὶ κάλλος αὖὐ-
µου πορεύσῃ, καὶ εἰς τὴν φυ2ήν µου, xal Aívn ταῖς ':Ε;ι.'ί];ζιικσι>πι fjv , οὐδεμιᾶ; ἀῤῥωστίας ἐναχλούσης
γυναῖκα τῷ υἱῷ pov. 'Αλλ ἵνα μὴ πᾶσαν τὴν ἵστο- π; =τιι.ν!ξ:.α'=ι. ἀλλὰ πάσης βλακείας ἐκθεθλημένης. Οἱ
iav εἰς µέσον φέροντες παρενοχλεῖν δόξωμεν, ἐπὶ τὸ γὰρ πόνοι χαὶ al -:akammpifx'. καὶ τὸ ἐν …… αὐτ-
ρ ᾽ φέροντες παρενοχλεῖ: BEV t*^79 — ουργεῖν βλαχείαν μὲν ἀπήλαυνε πᾶσαν , εὑρωστίαν
τέλος ἵωμεν. Ἐπειδὴ γὰρ εἶπε, πῶς ἐπὶ τῆς πηγῆς — [227] δὲ καὶ ὑγείαν μετὰ πολλῆς παρεῖχε τῆς ἀσφα-
ἔστη, πῶς ᾖτησε τὴν κόρην, πῶς ἔδωκεν ἐκείνη πλέον λείας, Διὸ δὴη καὶ ποθεινότερα: τοῖς ἀνδράσιν ἦσαν,
τῆς αἰτήσεως, πῶς 6 Θεὸς µεσίτης ἐγένετο͵ καὶ πάντα Χαὶ μᾶλλον ἐπέραστοι: οὗ vàp óh vb σῶμα µόνον,
μετὰ ἀκριθείας ἀπαγγείλας , χατέλυσεν iv τοῦτοις ἀλλὰ καὶ τὴν φυχῆν τούτοις βελτίω καὶ σωφρονεστέ-
[336] τὸν λόγον. Ταῦτα ἅπαντα ἀκούσαντες Exslvot, )Ρ 2v χατειργάσαντο. |(€!(]τ]ι1-…*:3|'|] τοίνυν ἐπὶ τῆς ΐ': ?
οὐχκέτι λοιπὺὴν ἀμφέθαλον, οὐδὲν ἐμέλησαν, &X äo u.xa.l φθάσασα εἰς τὴν XDp2Y ζτρ1ν- ώτήΐ ἐλθεῖν
- - - . ν οΔ γγὺς , ἀναθλέψασα εἷδε τοῖς ὀφθα)μοῖ- τὸν Ἰσαὰχ,
ὥσπερ τοῦ Θεοῦ τὴν φυχὺν αὐτῶν εἰς τοῦτο κινῆσαν- Ἆαὶ κατεπήδησεν ἀπὴ τῆς καµήλου. Εἶδες ἰσχύν;
τος, εὐθέως αὐτῷ καθωµολόγησαν τὴν θυγατέρα. — εἶδες εὐεξίαν; Κατεπήδησεν ἀπὸ τῆς καµήλου. Οὕτως
Καὶ ἀποκριθεὶς Λάδαν καὶ Βαθουῦλ εἶπον- Τὸ πρὀόσ- …'…τΞιτ; #ηΞη πολλὴ ΐ=ά ωφρ;σύνης ζν Καὶ ?== τῷ
rapyua τοῦτο πωρὰ Θεοῦ ἐξῆλθεν οὗ δυνησόµεθα — 77 9U Τίς ἐστιν' ὁ ἄνθρωπος ἐκεῖνος ὁπορευόμενος
οὖν σοι ἀντειπεῖν xaxór* ἰδοὺ ἹῬεδέχκα , .ἰαδὼν -- πρ.πεδιφ; Εἶπε δὲ ὁ παῖς- Ρ χύριος pov. bhe
Ν , Ja6ovca τὸ θέριστρον περιεδάλετο. "Opa πανταχοῦ
ἀπότρεχε, xal ἔσται γυνὴ τῷ κυρίῳ σου, καθάπερ «ὶν σωφροσύνην αὐτῆς μαρτυρουμένην , πῶς fv al-
ἑάλησε Κύριος. Τὶς οὑκ ἂν ἐκπλαγείη; τίς oüx ἂν σχυνττρὰ, πῶς ἦν αἰδέσιμος. Kal ἔλαδεν αὐτὴν d
θαυµάσειεν, ὅσα xal ἡλίκα κωλύματα iv βραχείᾳ  Ἰσαὰκ, καὶ ἐγένετο εὐτῷ elc γυναῖκα καὶ ἠγάπησεν
καιροῦ ῥοπῇ ἀνῃρεῖτο; Kal γὰρ τὸ ξένον εἶναι, καὶ «ὑτὴν, καὶ παρεχ.λθη περὶ Σάῤῥας τῆς μητρὸς αὖ-
οἰκέτην, καὶ ἀγνῶτα, καὶ πολὺ τῆς ὁδοῦ τὸ διάστηκα, ?" α΄ξ 'ί1΄"΄)'ι'*ΐ΄…δ'ξ'()"'΄"'Σ'-τ*"3ει΄ΐ>…΄'Ι""'*"ι ὅτι ἠγάπησεν αὖ-
καὶ tb μηδὲ τὸν κηδεστὴν, μηδὲ τὸν νυμφίον͵ μήτε νν ζ=ΞΡΞχΛή ? περ: της Σάῤῥας τῆς μ' ψΘε
. UA -« (ξεσθ αὐτοῦ, ἀλλ' ἵνα µ.ά0:η, τοῦ φίλτρου καὶ τῆς ἀγάπης
ἄλλον τενᾶ των Ηε.€νμ 5 τ:ροσηγ,6?των γνωρίξεσθαι, -ᾱς ὑποθέσεις, ἃς οἴκοθεν ἔχουσα ἦλθεν ἡ γυνή. Τίς
καὶ καθ' ἑαυτὸ τούτων ἕκαστον ἱκανὸν fv χωλῦσαι γὰρ τὴν τοιαύτη» οὐκ Àv ἠγάπησε, τὴν οὕτω σώ-
. τὸν γάµον: &AX ὅμως οὐδὲν ἐκώλυσεν, ἀλλὰ πᾶντα φρονα, vv οὕτω κοζμίαν, τὴν οὕτω φιλόξενον καὶ
ταῦτα ἐξηυμαρίζετο καὶ χαθάπερ γνωρίμῳ καὶ πλη- - φιλάνθρωπον xat ἧμερον, τὴν ἀνδρείαν μὲν κατὰ τὴν
αἴον οἰκοῦντι, xal ἐκ πρώὠτης ἡμέρας συναναττρα- ψυγὴν, ῥυμαλέαν Ob κατὰ τὸ σῶμα; Ταῦτα εἶπον,
φέντι, οὕτως αὐτῷ θαῤῥοῦντες ἐνεχείριζον τῆν νύμ- — 97X ἴνα ἀκούσητε, o20 ἵνα ἀκούσαντες ἐπαινέσητε
φην " 1b δὲ αἴτιον, 6 Θεὸς fv bv μέσῳ. “Ὥσπερ γὰρ  ΠΜόνον, ἀλλ᾽ Pa ….ζηλώ=ητε- Kai πατέρες μὲν, τῆν
ὅταν χωρὶς αὐτοῦ τι ποιῶμεν, κἂν ἅπαντα ᾗ λεῖα ναὶ ':τ,&ο-(5'|ο|.ιιπ:.".'τ.'; ταατριάρχον µ.ιμεϊ:9ε, ἣν "…ΞΞΐΌ Μξ*-
ῥᾷδια , βάραθρα xaX χρημνοὺς xai μυρίας &v αὐτοῖς του Μ56Ξ… Tovana änxaatov,. ou'xpf]y.a'.a.' Ξπιζητ'η-
ἀποτυχίας εὑρίσχομεν: οὕτως ὅταν mapf, χαὶ συν- ob %νοιξ; Μιµπρ-ότηΞα. QU σωμάτος Χάνος, OUX
; . ἄλλο οὐδὲν, ἀλλὰ ψυχῆς εὐγένειαν µόνον : μητέρες δὲ,
ΐ'ιρείτι:':':ι".αι.->ιΞ*; άπ.έ…)'ϊι:)-: ἀπορώτερα &:3 '::…ου»'ζ.εί*ιε*|.'ι, οὕτω τὰς ἑαυτῶν ἀνατρέφετε θυγατέρα;. Oi δὲ
ἅπαντα λεῖα χαὶ ῥᾷδια γίνεται. Μηδὲν οὖν μῆτε ἄγεσθαι αὐτὰς μέλλοντες νυμφίοι, μετὰ τοσαύτης
ποιῶμεν, μήτε λέγωμµεν, πρὶν ἣ τὸν Θεὺν καλέσαι αὑτὰς ἄγετε χοσµιόνητος, χορείας μὲν καὶ γέλωτας -
καὶ παρακαλέσα: συνεφάγασθαι τῶν kv yspolv ἡμῖν καὶ ῥῆματα αἰσχρὰ, καὶ σύριγγας καὶ αὐλοὺς, καὶ
ἁπάντων, καθάπερ οὖν χαὶ οὗτος ἐποίησεν. τὴν διαθολικὴν φαντασίαν, καὶ xX ἄλλα τπάντα τὰ
0. ᾿Αλλ᾽ ἴδωμεν, ἐπειδὴ αὐτὴν ἔλαθε, ποίῳ ζ:ώ… τ(ι|.:..'..;'.α ΞορίζοντΞς: τὸν Θεὸν δὲ ἀεὶ π;αρ΄παλώντε;
τὸν γάµον ἐπετέλεσεν. "Apx κύμθαλα καὶ σὐριγγας καὶ ῳἈΞσΐτην γενέσθαι τῶν πραττομένων ἁπάντων. Ἂν
χοριὺς καὶ τύμπανα καὶ αὐλοὺς καὶ τὴν ἄλλην ἑπεσύ- Ὑὰρ οὕτω τὰ καθ᾽ ἑαυτοὺς οἰκονομήσωμεν , οὗκ ἄτο-
ρατο φαντασίαν; Οὐδὲν τούτων , ἀλλὰ µόνην λαθὼν στάσιον ἕἔσται Μτ.ξ, 00 µοιχείας …τοψ.ία , οὗ Θλοτυ-
οὕτως ἀπῄει, τὸν ἄγγελον ἔχων μεθ’ ἑαυτοῦ παρα- — 7525 πρόφατις, οὗ páym καὶ ἔρις, ἀλλὰ πολλῆς μὲν
πέµποντα αὐτὴν καὶ συνοδοιποροῦντα , Üv ὁ δεσπότης ἀπολαυσόμεθα τῆς εἰρήνης, πολλῆς bb τῆς ὁμονοίας”
αὐτοῦ vbv Θεὸν [χέτευσεν αὐτῷ συναποστεῖλαι ἐκ τῆς "αὐτῆς δὲ οὔσης, καὶ ab ἄλλαι πάντως ἕφονται
οἰκίας ἐξερχομένῳ. Kat ἤγετο λοιπὸν ἡ νύμφη αὐλῶν — 2052* Ὥσπερ γὰρ, μναικδς- zph; ἄνδρα στασια-
plv καὶ κιθάρας καὶ τῶν ἄλλων τῶν τοιούτων οὐδενὼς  Ὀθύσης, οὐδὲν ὑγιὸς ἔσται ἐπὶ τῆς οἰκίας, κἂν ἅπαντα
ἀκουσασα, µυρίας δὲ παρὰ τοῦ Θεοῦ ἐπὶ τὴν χεφαλὴν τὰ ξϊ"λ΄* κατὰ Ροδν ΨΐΡΨ… 1ΞξΞ΄ι΄)΄!'…α"ΐ" * οὕτως Ψ-…0'
τὰς εὐλογίας ἔχουσα, στέφανον διαδῆµατος παντὰς ''"Αύύσης καὶ εἰρηνευούσης, οὐδὲν ἔσται ἀτδὲς, xàv
λαμπρότερον."Ηγετο οὗ χρυσᾶ περιθεθληµένη ἱμάτια,  μορίοι µιμωνες΄χαθ ἑχάτστην τίχτωνται τὴν ἡμέραν.
ἀλλὰ σωφροσύνην καὶ εὐλάθειαν, καὶ φιλοξενίαν, καὶ — ^Y οὗτως οἱ γάµοι γένωνται, xal τὰ παιδία μετὰ
τὴν ἄλλην ἅπασαν ἀρετήν. "Ἡγετο οὐκ ἐπὶ ὀχήματος ολλῆς τῆς εὐκολίας zii ἀρετὴν ἀγαγεῖν δυνησόμεθα.
χαταστέγου, οὐδὲ ἐπὶ ἄλλης τινὸς φαντασίας ποιαύ. Ὅταν γὰρ ἡ μήτηρ οὕτως jj xospía, καὶ σώφρων,
της, ἀλλ᾽ ἐπὶ τῆς χαµῆλου καθηµένη. Μετὰ γὰρ τῆς καὶ πᾶσαν ἀρετὴν πχτημέΐη. πιντω;-χα! τὸν ώζδρα
ἀρετῆς τῆς Ev τῇ ψυχῇ, καὶ τὰ σώματα ταῖς παρθέ- ἑλεῖν ωνΌσεται ΐα*. µιµυσα50;ι'τψ ='."..=.ρ'ν.. αὐτὴν
νοις τὸ παλαιὺν πολλῆς μετεῖχε τῆς εὐέξίας, Οὐ γὰρ …ΐ "'ελοῦσα 8b αὐτὸν, μετὰ πολλῆς ἕδει τῆς προ-
οὕτως αὐτὸς ἔτρεφον al μητέρες, καθάπερ τὰς νῦν, ωξ 3 %η.ξουντ.α πρὸς τὴν των παίδων Μ;µέλΐαν.
βαλανείοις πυχνοῖς, μύρων ἁλοιφαῖς, ἐπιτρίμμασι "-,' " ?΄ ω:: ")ξ Ξ΄(…σπέ: Έται πρὺς τ äv N Ψ*΄
σχιαγραφίας, μαλακεῖς ἱματίοι;, ἑτέροις µυρίοις δια- ΐ.;ξτ3νταξζνοΖνηΜ ΐ;Τω } ;τυΐαζτ ζ!;ι, a (æc
ς=θιί-ρσυσαι ψ'όποι;. καὶ τοῦ δέοντος μαλαχωτέρας äm'äm, ζςωιξ &ιζς"!;|τλοξπδγΈ Ώ'λ; ::ι Ή;,
ΐ';΄΄""" ?ιλ "; ἅπασαν αὐτὰς Ίγαγον ἐκεῖνα:'.-της οἰκίας εὖ διακείσεται, τῶν ἀρχόντων οὕτω [998]
σκληραγωγίαν. &…- Τοῦτο ανταις ?*1ι του σώµατος ἡ — διαχειµένων καὶ μετὰ τῆς οἰχίας οὕτως ἕκαατος δυ-
ὥρα σφόδρα ἣν εὐανθῆς καὶ γνησία, ἅτε φυσική τις νῄήσεται, τῆς ἑαυτοῦ γυναικὸς λέγω, χαὶ παίδων καὶ
* Savil. in marg. conjicit ἐχείνῳ. οἰκετῶν, καὶ τὸν ἐνταῦθα μετὰ ἀδείας ἁπάσης διανύ-
