ι
5 Β. JOANNIS CHRYSOSTOMI ARCHIEP. CONSTANTINOP. l 5176
iHorum  experius e6? Αο non tum etam, eum ος — omnes, qui primus rpsorum furori ausus fuerat re-
adessent millia Jdmorum , nonne eadem. esga te usi.— sistere ? .
suntsapientia ? noune te seorsim assumpto tibi dixerunt, δ. Hoc enim demum est eximium, mon qued fuerit
Vides frater , quot millia sunt Judaorum , qui consene-— Christum confessus, sed'quod ante ewteros omnes,
runi, et hi omnes amulatores sunt legis, et audierunt de — cum illi furerent adhuc e:xde tumefacti , confidenter
le, quia discessionem doceas α lege. Quid ergo est* — confessus fuerit. Ut igiturin bello atque acie, cohorte ᾽
Fac quod tibi dicimus. Sunt inter nos viri votum habeues — communita, illum przecipue laudamus, qui ante cze-
super ες : his assumptis vadere cum illis, et sanctifica l& — teros prosiluerit , ejusque frontem perruperit ( non
cum illis, ut discant , quia qua ς te audierunt, falsa — enim hujus tantum rei, sed et omnium , qux cxterb
sunt (Act. 91. 20.-24). Vides nt existimationi (ά patrarinb , przeclarorum facinprum causam przbuisse
parcant? ut (te dispensotionis illius sub larva o€-  dicelur , qui initii et. ingressus auctor fuerit ) : sie
eultent, et sacrificio ac sanctificationibus teobtegant? — nimirum et ἀς Ῥοίτο reputare oportet, cum primus.
Cur non eamdem tu quoque sollicitudinem exhibes? — ingressus sit, et frontem Judaicz cohoriis perruperit,
4. Énimvero si vere pugna esset res ista οἱ con- — ac prolixam illam eoncionem habuerit, pari ratione
tentio, bis accusationibus locus esset: jom vero viam czleris apostolis aperuisse dicendus est, Quamvie.
cerium est non esse pognam, sed videri quidem, ma« — Joannes, quamvis Jacobus, quamvis Paulus, quamvis
gnam vero Pauli et Peiri sapientiam, et inter ipsos — alius quivis deinde facinus aliquod egregium ediderit,
mutuam benevolentiam prz se ferre. Interim tamen — hic omnibus antecellit, qui. viam czeteris. sua fiducia.
hanc, quz talis videlur esse, accusationem audia- —stravit, et ingressum aperuit, illisque concessit, ut.
mus. Cum autem venissel Petrus Antiochiam, in faciem — tamquam flumen, quod magno oum undarum impetu.
. ei restiti, Quam ob causam? Quia reprehensibilis erat, — ferretur, confidenler intrarent,, et οος qui' adversa»
Quodnam autem genus est reprehensionis? Priws — rentur secum traherent , corum vere qui benevole
enim, quam cenirent. quidam a Jacobo , cum gentibus — andirent, animas perpetuo migarent. An igitur ille.
edebai : cum autem venissent , subtrahebat εἰ segregabat — post crucem talis fuit? nonne ante crucem cunetis.
&e, limens εος qui ez circumcisione erant. Quid ais? — fuit ardentior ? nonne os fuit. apostolorum ? nonme si-
Timidus Petrus, et parum strenuus? Annon idcirco — lentibus cunctis ipse loquebatur? (Quen me dicunt ho-.
Petrus est appellatus, quod immobilem baberet fidem ? — mines esse Filium hominis ( Matth. 16. 15) ? ait Chri-.
Quid agis, mi homo * Reverere nomen, quod discipulo — stus ; et aliiquidem Heliam dicebant, alii vero Jere-
imposuit Dominus. Timidus Petrus et parum stre- — miam, alii vero unum ex prophetis. Vos vero, quen
nuus? Et quis hoc ab ullo sinat affrmari? Non hxc — sme esse dicitis (Ibid. v. 16) ? inquit. Tum respondens
de illo testari. potest Jerosolyma, primumque illud — Petrus , dixit: Τν es Christus. Ειῆκε Dei vivi. Vos,
theatrum , et Ecclesia, in quam primus prosiliit, et — dixit, e& pro omnibus ipse loquutus est. Nam quem-
beatam illam vocem primus emisit ac dixit : Hunc — admodum pro 1oto corpore os loquitur, sic aposto-
Jesum Deus suscitavit, solutis doloribus inferni (Actor. — lorum lingua erat Petrus, et pro omnibus ipse respon- .
9. 94). Et rursus, Nox. enim David ascendit in ce- — dit. Àn igitur hie tantum fuit talis, alibi vero de boc
lum. Dicil ακίεηε ipse, inquit, Dixit Dominus Do- — studio remittit? Nequaquam : sed per omnia , et in
mino meo, Sede a dextris meis, donec ponam inimi- — omnibus eumdem ardorem ostendit. Cum enim Chri-
cos tuos scabellum pedum tuorum (1b. v. 56, 35.) 5Lus dixisset, Traden: Filium hominis, εἰ flagellabunt,
Petri constaniia in Ghristo predicando ; Petrus aliis — & crncifigent ( Marc. 10.55. 54), ait ipse, Propitius
apostolis fiducia antecellit. — An ergo timidus est iste — tibi , esto, Domine : non erit tibi hoc ( Mauh. 16. 24 ).
36 parum sirenuus, qui tanto metu, tantisque peri- — Non enim hoc nobis expendendum est, iuconsidera-
eutis impendentibus , tanta cum fiducia ad illos san- — tam hanc fuisse responsionem , sed ab eximio et
guine pastos canes , ardenies ira, czedem adhuc spi- — ferventi amore profectam. Rursus ascendit in mona-
rantes est ingressus, ac dixit eum, qui ab ipsis füerat — tem , et transfiguratus est : apparuit "nierim ἀε
crucifixus, et resurrexisse a mortuis, et in ezlo ver- — ]lelias et Moyses colloquens. Rursus illic quoque
sari, ad Patris dexteram sedere , ac malis innumeris — Petrus : δὲ vis, faciamus hic tria tabernacula ( Matth.
inimicos suos obruere ? Non tu illum potius, quzse, — 17. 4 ).
müraris, et in. cx»lum effers, quod os diducere, quod 6. Vide quomodo magistrum amabat, ac diligen-
. abraaperire, quod.stare, quod comparere solus cum — tiam et prudentiam οὕθογνα, Nam quia tuim temporis, :
illis potuerit, qui eum cruciflxerant? Quis enim ser- — cum temere respondisset, os illi occlusum est , hic
mo, qu:* mens flduciam, quam illo die pr» se tulit, — magistri potestali rem permittit, dicens, Si vis. Fieri
ac loquendi libertatem exprimere poterit? Nulla om- — posset, inquit, ct nunc quoque inconsiderate loqueres
nino. Si enim ante passionem conspiraverant Judzi, — amore impulsus. Νο igitur eadem reprelrensione fe-
ut sí quis eum confiteretur esse Christum, extra syn- — riatur, Si vis, inquit. Convivium illud rursus erat
agogam fleret. ( Joan. 9. 22) : post passionem et — sanctum ac tremendum : tumque dicente Jesu, Unus
sepultaram audientes eum, non qui Christum confl- — vestrum. me traditerus. est ( .Mauth. 96. 21 ) : rursas
teretur solum, sed et totam simul dispensationem — Petrus ob illam quidem quz przecesserat, increpatio-
$umma constantia predicaret , quo pacio non di- — mem, interrogare magistrum non ausus est : sed ob
lacerarunt, ct. membratim concisum diviserunt eum — amorem, quo flagrabat, taccre non potoral : sed-oi
