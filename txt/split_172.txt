179 S. JOANNIS CIIRYSOSTOMIl ΑΠΟΠΙΕΡ. CONSTANTINOP. . 180
nientem orationi, audi quo pacto apostolus e! pro- — enim ut maxime memores simus tempus exigit ; et
plieta in illum invehantur. Nain hic quidem sic ait: — quemadmodum athleta, qux in palzstra didicerit ,
Peccatori autem dixit Deus : Quare tu enarras jusiitias — eorum in certaminibus specimoen ο : aie nos etiam
meas , et assunis lestamentum. meum per 0s tuum , tu.— qux& hic audimus, in externis negotiis oportet 08
vero odisti disciplinam ( Psal. 49. 16. 11 )? Apostolus — tendere.
Autem cum in eosdem istos rursus invehitur , qui ob 9. Memor igitur eorum esto quie hie dicuutur, ut
doctrinam magnifice de se senliunt, ita loquitur : — Cutn egressus fueris, et te diabolus vel per iram, vel
Confidis teipsum esse ducem cacorum , lumen eorum, — pcr inanem gloriam, vel peraliam aliquam passioneia
qui in tenebris aunt , eruditorem insipieniium , magi- — invaserit, ejus doetrinze memor, qua liic. imbutus es,
strum infantium. Qui ergo alium doces, teipsum naon facile pussis mali laqueos et nexus evadere. Nonne
doces ( Rom. 2. 19-21)? videtis in stadiis pzedotribas ac pabestrze magistros, qui
Lectioni Scripturg addantur bona opera. — Quaudo — cum post mille certamina vacationem deinceps a lu-
igitur neque mihi qui dico, prodesse polest, quod — Ctis beneficio ztatis aeceperint, extra stadium non
verba faciam , neque vobis audientibus , quod audia- longe ab ipso puivere scdentes iis qui sunt intus ac
tis, nisi dictis obtemperetur , sed et. magis conde- decertant elaniando suggerere, ut manum prebendant,
mnat : ne sola auditione tenus studium exhibeamus , — Ut crus attrahant, ut dorsum invadant, et alia multa
sed operibus etiam ea qui dicuntur observemus. — ejusmodi monentes, si hoc vel illud fereris, adversa-
Bonum q"ide]n est assiduam sacrorum eloquiorum rium facile in terram extendes : hac ratione diSCipll- .
auditioni operam dare ; sed et hoc bonum inuti'e red- — ἰς᾽ maximopere prodesse? Tu quoque ραάοιτϊνασι
ditur , si conjunetam utilitatem non habeat, α ex — luum beatum Paulum intuere, qui post corouas innu-
obedientia nascitur. Ne igitur hic frustra coaveniatis, — meras, exMra stadium uunc consideus, hoc est, ex
omui studio , quod a. vobis precibus smpe contendi , — hac *i.a egressus eertantibus nobis clamando suggerit,
neque contendere desinam , fratres ροδῖς adducite, — et per epistolas suas vociferatur, cum correptos ab ira
cohortamini errantes , cousilium date non verbo lan-— viderit, ab injuriarum memoria, atque a. passionibus
tum, sed etiam opere. Major est ista doctrina que — oppressos, Si esurierit. inimicus tuus, ciba illum (Rom.
moribus , φµ couversotione profertur. Etsi nihil di- — 12. 20). Et quemaduiodum dicil pal:estrz: magister,
Cas , si tantum exeas e collecta , et externa specie, — &i hoe velillud feceris, adversarium superabis, iia
- aspectu , voce , incessu , et tola reliqua medesta cor- — subjicit μο quoque : [Joc enim . faciens, carbones ignis
poris compositione lucrum , quod hine egressus te- — congeres super caput ejus. Verum dum legem percurro,
cum retulisti , ostendss hominibus, qui eollect:» non — quzstio nobis oceurrit, quis ex ipsa masci videtur,
interfueruut , ad coliortationem et consiliuin id suffi- — et multis adversus. Paulum reprehensionis ansam
€it. Sic enim egredi πος Μς oportet , quasi ex sacris — suppeditat, quam hodierno die statui vobis in medium
adytis , quasi de cxlo delapsos, modesliores reddi- — afferre. Quid ergo tandem ecst, quod mentes eorum
tos , philosophantes, moderate ac temperale cuncta — !crret, qui diligenter omnia nolunt excutere? Cum ab
facientes αο dicentes : el uxor maritum a collecta re- — ira revocaret Paulus, inquit, et ut mansucti. essent,
cedentein videus , et filium pater, et patrem filius, — ac wodesti erga proximos, suadcret, illos amplius
et dominum scrvus , et amicum amicus, et initiicum — Cxasperavit et iracundia inflammavit. Quod enim -
inimicus , omnes utilitatis seusum capiznt , quam ex — xit, Si esurierit inimicus tuus, ciba illum; si aitit, potum
hoc conventu reportavimus : capient autem , εἶ mitio- — da i/Ii, przelarum mandatum est, ct philosophize ple-
res, si patientiores, si religiosiores vos factos anim- — num, ac tum ageuti, tum patienti perutile : quod
advertint. Cugita quibus mysteriis interesse libi — vero deinde sequitur, imultam habet. difficultalem,
datum sit, qui initiatss es, cum quibus my ticum — minimeque videtur cum ejus, qui priora dixit, sen-
illum cantum offeras, cum quibus ter sanctum hy- — tentia convenire. Quidnam vero illud est ? Quod dieat,
mnum pronunties. Doce profanus, te cum Seraphim — JHoc autem faciens, carbones ignis congeres super caput
cliorcas agitasse , te ad czelestem populum pertiuere, — ejus. is enim verbis et eum qui agit οἳ eum qui
te in chorum angelorum adscriptum esse, te cum — patitur afficit injuria, cum istius caput exurat, et ignis
Domiuo colloquutuim esse , te cum Christo esse con- — carbones imponat. Quod enim tantum bonum obve-
gressum. Si nos ità composuerimus , nihil oratione — niat, ex οο quod cibeiur et potelur, quantum  nialum
opus erit erga illos, qui non interfuerunt, sed ex — cx accumulatiune carbouum ? Itaque illum qui. bene-
profectu nostro jacturam suam animadvertent , et — ficio afficitur, inquil, hoc pacto aflicit injuria, eum in
| coufestim accurrent , ut. iisdem commodis perfruan- — majus supplicium illum coujiciat ; eum vero qui beue-
; tur. Cum enim aninue vestrz: pulehritudinem ex ipsis — ficium contulit, alio rursus iodo krdit. Quid enim -
; sensibus vidcrint relucentem , licet omnium sint stu- — referre lucri potest ex eo, quod de inimicis bene me-
pidissimi , ad cupiditatem eximii vestri decoris exar- — reatur, cum illud spe supplicii faciat? Nain qui id-
| descent. Si euim forma corporis excitat intuentes, — circo cibal vel potat inimicum, ul in caput cjus car-
. multo magis anim:e pulehritudo spectatorem potest — bones congerat, non benignus et clemens, sed crude-
commovere,, atque ad similem zelum hortari. Intc- — lis est et immitis qui per cxiguum beneficium sup-
1 riorem igitur hominem nostrum exornemus, etco-  plicium immeusum inferat. Quid enim asperius fieri
i Τυ α hie dicta fuerint foris recordemur : illic — potest co, qui propterca nutriat , ut in capul cjus qui
l
