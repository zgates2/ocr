15 :S. JOANNIS CHRYSOSTOMI ARCITIEP."CONSTANTINOP. 314
aliud longe opportunius dicam, quo ineluctabiliter tantes et refugientes ita respondent : Si Bewt véluertt,
demonstratur Paulum non ex necessitate ad Dominum — persuadebit milii, e& convertar. Sane.non- eriminor
mecessisse. Venit postea:Paulus Salaminem Cyvpri,  illos, sed admedum approbo, quod ad .voluntatem
fbique invenit magum quemdam sibi obsistentem — Dei confugiant ; sed volo ut ea, αο penes ipsos sunt,
"pud proconsulem Sergium. Paulus vero repletus — afferant, οἱ sic dicemt, Si Deus veluemt. Nam si te
Spiritu sancto dixit illi ::O plene omzi dolo, οἳ omini — sompo ac segnidei dedens, non ceneris bona edere
versutia, fili diaboli, non desines pervertere vias Domini epera, sed Dei tantwm voluntatem objeteris, nihil
rectas (Act. 15. 10) !* Hxec persequutoris verba. sunt. eorum, quibus opus habes, adipisceris umqunm,. Nam,
"Gloriam Domino referamus, qui ipsum convertit. An- — ut dicebam, netessitate ac-vi neminem umquam adi4
iea audiebatis, quod devastaret Eeclesiam ,in do- git Deus : sed vult quidem omaes salvos fleri, nemini
mos ingrediens, tralensque viros ac mulieres abripe- ^ vero necessitatem inducit, quemadmodum οἳ Paulns
ret in carcerem. Videte quanta cum fiducia nunc pro — dicit : Qui vult omnes homines salvoe fferi, et ad agri-
pradicatione loquatur : Non. desines, iuquit, pereer- — tiomem «veritatis-wenire (1. Tim. 9. 4).Quemede igitur
tere οἷας Domini rectas ?. Et . nunc. ecce manus Domini — gon.omnes salvi fiant , si vu't omnes salvos fleri?
"auper te : et eris ca'cus, et non videns usque ad tempus. Quia non omnium voluntas ejus. voluntotem sequitür;
Tdem remedium mago -imposuit, quod.sibi ipsi visum . ]ο vero memini vim infert, Sic et ad Jerusalem di-
veslitueraL ; sed. ille mansit in exeitate : ut díacas — cit : Jerusalem, JM»:, quoties. wolui congregare
Ton vocationem n!odo Paulum adduxisse, sed ctiam filios ttos, ε woluiatis (Luc. 48. .Db) Τ Quid: igitue
eius-proposilum Yolontatis. Nam si czcitas sola id — "Ecce relinquetur domets. vestra. deseria ( 15. v. 55).
*effecisset, in mago etiam idipsum evenire oportuisset : Viden' quod eisi Deus velit πος saivos facere , nos
sed non ita faetum est. Verum ille quidem exczcatus — ,utem aceedere polimus, in interitg maneamus
*esi, proconsul autem videns id, quod factum erat, — Νο ή invitum nonnol;næm.-usæpe repeto, sed
-credidit. Alius remedium accepit, et alius-visum re- libentem ei'-volem,em homigem ut salvum facíat Όος
euperavit. Videle quantum bonum sit :probus animi paratus est. Homines quidem :famulis volentibue ne-
:alfcctus ; quantum vero malum inobsequentia et cordi& — jontübus dominari et imperare vólunt, non ο serve-
-durities. Exczecatus est magus : et ille nihil lncratus — .puea utilitatem, sed ad suam respicientes : Deus vero
»es8t, quod inobsequens fuerit ; proconsul vero Chri- -qui nemine in'djg-e[, cum tibi demonstrare velit se
*tum cognovit. Quod igitur sponte οἳ ex proposito nulla rerum nestrarum indigentem , servitutem no-
svoluntatis aceesserit Paulus, id satie jam demonstra- siram expelere, sed ad utilitatem ,solum nostram
tumn est, ld-vero jam volo, νος id probe nosse, Deum respicere ; € non ad usum suum, sed ad emolumen-
mempe non vim inferre ltolenlibus, sed volentes lra- — um noslr:lm &e omnia facere : ni,si sponte ac volen-
Mb sr ν ν Ὅ α νν quialperqiigegent
(’ο )- Qui vero trahit, — {ηνίιος ac regilientes numquam cogit nec νἱ΄ adigit.;
volentem traliit, humique jacentem, ac manum porri- — oatendat, non 60 nobis gratiam servitutis, sed nos
gentem. Et ut discatis ipsum nemini vim ferre, ct si ipsi gratiam: babituros esse dominii. ἀσο itaque.cum
ipso volenté nos nolimus, qu:x ad salutem nostram seiamus, Domini clementinm considerantes, dignum
speclant pessum ire ; non quod ejus voluntas infirma ejus benignitate, quantum facultas eeit, vit instin-
sit, sed quod nemini velit neceasitalem inferre : hanc — (u, exhibeamusg, ut. et regnum -cxlorum - consequa-
rem dssquirere necesse est, quoniam multi smpein — ,. quo nos consortes fieri coutingat, gratia et
segnitiei sux obtentum lhac futili utuntur defensione: — pimenitate Domini nostri Jesu Christi, cui gloria-et
sepeque moniti ut illuminalionem seu baptisina sus- imperium, una eum Patre οἳ sancto Spirilw, nunc οἳ
eipiant, vitz institutum in melius comrutent, alia- semper, et in secula sceulorum. Amen.
que similia bona opera exsequantur, tunc illi pigri- —— —
REPREHENSIO
ΕΟΒυΝ, QUI ΑΒΕΒΑΝΤ AB ECCLESIA, ET COHORTATIO AD PILESENTES, UT CURAM GERANT Ἐδὰ--

TRUM : ET IN PROOEMIUM ΕΡΙΘΤΟΙ Ε AD CORINTHIOS, PAULUS VOCATUS (1. Cor. 1. i), kv D&

HUMILITATE.

4. De paucitate auditorum loguitur; umor. propria — dinem excitarit, Propterea beatos ves predico, εἰ
fncit. φ communia, Tabernacula peccatorum domus — wmulatione dignos censeo, quod nihil vobis illorum
privaie. — Cum ad vestram paucitatem oculos meos — nocuerit negligentia : miseros autem illos przedico,
converto, et colleetis if singulis gregem nosirum — ct lacrymis prosequor, quod nihil eos studium vestrum

' minorem fleri cerno, mdreo simul et gaudeo : gau- — poluerit adjuvare. Non enim propletam audierunt
deo quidem propter vos, qui adestis : moreo vero — dicentem : Elegi abjectus esse ν domo D& magis ,
propter illos, qui absunt. Nam vos quidem laudibus — quam habitare ν tabernaculis. peccatorum (Psal.. 83.
digni estis, quod nec ipsa paucitas negligentiores νος — 11). Non dixit, Elegi habitare in domo Dei mei, neque
reddiderit : illi vero criminibus sunt obnoxii, quos — versari, neque ingredi ; sed, Elegi abjectus esse. Licet
néque stadium vestzum ad majorem animi promptitu- — inter ultimos numerer, boni consulo, hac re conten-
