W — Á——À— — ——RRU——Á (( — Ini—olmnaiun iUl
T ricfati
. IN TERTIUM TOMUM OPERUM
S. JOANNIS CHRYSOSTOMI.
εοο ol

Ea fuit instituti nostri ratio in hac paranda S. Joannis Chrysostomi Operum Editione, ut
Opuscula primum omnia, cujusvis Iäeneris essent , in tribus Tomis prioribus locaremus; ia
sequentibus vero Commentaria in Novum et Vetus Testamentum. Opuscula porro omnia in
quinque classes distribnimus. Prima classis complectitur ea Opera, 4υ ob varietatem ar—

umenti non possunt uno titulo comprehendi, sed secundum temporis ordinem, quoad ejus
äeri potest, locantur. Secunda classis est Concionum paneäyricarum de Domino N. J. Christo
et de Sanctis. Hasce duas priores classes primus et secundus Tomus complectuntur. Tertia
classis est Homiliarum, 40 ex diversis Scripture locis argumentum mutuantur, αο Ἡο--
milie primam hujusce tertii Tomi partem occupant. Quarta classis, Opusculorum ad motus
Constantinopolitanos et ad utrumque S. Joannis Chrysostomi exsilium pertinentium. Quinta
classis est Epistolarum omnium. Tres iäimr posteriores classes in hoc terlio Tomo conti—
nentur, quibus subjiciuntur pauca queedam , et spuria denique multa. De illis omnibus hic
quadam pramittenda sunt.

$ Ἱ. — De Honiliis que εα diversis Scriptura locis argumentum mutuantur.

Ordimur ergo ab Homiliis in loca quedam novi Testamenti, quarum quatuor priores toti.
dem evangelicas sententias explicant. Octo autem sequentes α una serie et continenter
habite sunt , misere distracte et disperss erant in Editionibus Saviliana et Frontoniana :
quod cave seu incuris& seu negligentieSavilii aut Frontonis Duczi adscribas. Π enim nul-
làm non diligentiam adhibebant , non modo ut omnia Chrysostomi Opuscula ο omnibus
orbis Christiani partibus colligerent, sed etiam ut 3uæque 5ο ordine ponerent. Verum quia
jam prelo datis multis operibus, alia confertim undique accedebant , Βοο postremo advecta
plerumque ut fors ferebat locabant. Quanto autem lectoris dispendio Πος sic distracta ac
propemodum dissipata ederentur, statim animadvertet quisquis earum lectionem continefi-
ter peraget. Nam plerumque accidit ut in sequenti tantum prioris argumentum compleatur :
tantamque alioquin omnes habent inter se affinitatem , ut nonnisi preecedenti lecta sequen-
tem plane intelligere possis.

Ex hisce autem octo Homiliis quatuor priores sunt in principium, sive in titulum libri
AÁctorum. In hoc tamen argumentum quinque una serie conciones habuerat, ut non semel
testificatur Joannes noster : sed secunda, in qua quarebatur quis esset auctor libri Actàum
Apostolorum , intercidit. Illam porro integram me reperisse putabam, ul in Praefatione ad
primum Tomum dixi. Verum postea diligentius expensa homilia illa longissima , ex Mss.
eruta, que ἀρ Ascensione inscribitur , comperi partem quidem illam ubi , quibasdam de
principio Actorum ä)ræmissis, evidentissime probatur Lucam evangelistam esse Actuum
auctorem, esse haud dubje Chrysostomi : verum alia qua hanc partem pracedunt, et longe
plura 4πο illam subsequuntur, non gosse Chrysostomi esse. Haebam certe initio , quia
illa partem non contemnendam illius homiliz complectebatur, an in setie aliarum locanda
essel ; sed ingratum lectori fore putavi, si spuria illa alia que in eadem bomiliá occurrunt,
quorum quægam nugacem Graeculum sapiunt, veris operibus admiscerem : quamobrem to-
tam homiliam ad calcem hujus Tomi amandare visum est.

Quatuor item Homilie 4υ de Mutatione nominum inscripts sunt, quatuor illis in prin-
cipium Actorum nativo ordine connectuntur : in his quippe poslremis argumentum priorum
prosequitur Chrysostomus, ut in Monito diximus. Cietere Homilic in loca Pauli sunt , ac
cum pracedenlibus, triginta quatuor simul homiliarum numerum complent. Tres porro
Homilias de Legibus connubii, quarum prima est num. 18,inillud : Proptef fornicationes, etc.
altera num. 19, de Libello repudii; tertia num, 20, Laus Maximi, et quales ducenda uxores ;
has , inquam, Hormnilias , { unius et ;jusdem argumenti sunt et eodem tempore una serie
habitis, ut legentibus liquidum est, α Fronto Ducius ediderat, ut terlia a duabus prioribus
longo intervallo separaretur; sed θ suo ordine jam locate suas partes implere ac semcel
copiam materiam ad destinatum finem deductam lectori conjunctim offerre poterunt.
$ II. — Opuscula de motibus Constantinopolitanis, et de utroque Chrysostomi eznlió, qua—

nam sint.

Hec excipit quarta classis Opusculorum, tempe de motibus Constantinopolitanis, necnon
de aliis que ad atrumque Chrysostomi exsilium pertinent. Hec omnia secundum temporis
seriem edere visurn est , curn antea separata et tumultuario oEere diversis in tomis locata
essent , idque casu potius quam consilio. Ex iis non pauca in Editione Frontonis Ducsi de-

- χ» ParRor. Gn. LI. 1
χ(5/ : …:Ξ.. . 5 Ὄς π $QOU ::::5 iu E : u :::.. .
