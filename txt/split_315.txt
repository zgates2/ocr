321 IN ILLUD, VIDUA ELIGATUR. $31

IN HOMILIAM DE VIDUIS. ;
illam sententiam scrupulum injiciemus. Certum est 'Chrysostoluum anno ineupnte $87 non paucas ha-
buisse comciones de ὁμοουσίῳ et contra Anomeos : nec minus cerium solere Chrysostomum pratterita-
rem concionum meminisse : an vero contingere potuit , ut vigente illa disceptatione contra Ánomceos, in
nulla harumce novem homiliarum, qua sine dubio inter Homilias contra Anomoos inlermixta fuerunt ,
si illo anno dicts fuerint, de illa controversia. unquaw meminerit? Hac mihi nova suborta dubitandi
causa, animi pendeo donee quid oertius energat.

Interpretatio Latina est Frontonis Ducsei.

΄ IN ILLUD,
VIDUA ELIGATUR NON MINUS SEXAGINTA ANNORUM (1. T'ím. 5.9); ET ΠΕ LIBERORUM EDUCA-
᾽ * TIONE, AC DE ELEEMOSYNA (α).
00 — — QOBK- — —

1. Connexio dicendorum cum antea dictis. Árma — humanas lantum injurias, θ diinonum etiam ia
eiduarum φς απὶ, --- Commodum divini Spiriius — cursus poterit propulsare. Vidua a s»cularibus negotiis
gratia disponente contigil, ut is vobis apostolic:e lo- — immunis est, ad cxlum autem «deinceps teudit : et
εας epistolz, quem hodierno die audivistis, legere- — quod erga virum studium et cullum exhibebot, ο
tur : habet. enim cum iis quze nuper diximus, cogna- — in res spirituales potest impendere. Qued si dicas,
tionem et conuexionem haud mediocrem, οἱ minus — olim rem istam calamitatem [uisse, tum illud subji-
im verbis, ccrte in sententiis, Hoc enim illud est, — ciam, mortem quoque maledictam fuisse : tamen in
quod nuper legebatur, De dormientibus autem nolo vos — honorem ac dignitatem conversa est liis qui pauenler
ignorare, [ratres (M.Thess. À. 12) : et deresurrectione — tulerint invadentem. Sic nimirum et. inartyres corg-
inulta tum a nobis sunt. dieta, ήο animo ferenda — nautur, eademque ratione vidua sumimnam ad dignita-
esse mala ejusmodi, Deoque gratias agendas, qui — tem evchitur.
propinquos nostros sibi assumeret. llodie vero lectum 3. Vis intelligere, quanta res sit. vidua? quant sit
est istud : Vidua eligatur non minus serzaginta anuo- — apud Deum honore digna, et amore, utque summo-
rum (1. Tim. 5. ). Quando igilur ex occasione — pere suo possit apud Deum patrocinio juvare, οἳ cum
mortis viduilas oritur, et hoc est, quod maxime do- — primum apparuerit, jam damnatos, animum despon-
lorem auget, ac luctum exciat, memores eorum,  dentes, hiscere non audentes, odiosos Deo, omni
que nuper a nobis dicia sunt ad οος qui lugent con- — excusationis spe spoliatos liberarze, nc reconciliare,
solandos, eaque summo studio suscipientes, in vestrze— neque veniam tantum iinpetrare et supplieio exinere,
mentis promptuariis illa recondite. Nam viduilas qui« — sed et inullam fiduciamn ac. splendorem acquirere, ei
dem calamit:4is esse nomen videlur, sed tamen non — solis radiis ρυγίυγες reddere, licet ounium sint mor-
est, verum dignitas, honor, et gloria maxima; non — talium sordidissimi? Audi Deum ipsum Judzos οἷς
opprobriuu, sedcorona. Licet enim maritum non la- — alloquentem : Cum eztenderitis manus vestras, avertam
beat, cum quo habitet, vidua : tamen Chrisium habe$ — eculos meos a wobis : si multiplicaverilis orationem, non
cum 4υο habitel, a quo universa, quze ingruant, mala — ezawdiam vos : manus enim vesre songuine yleua
propulsentur, Sufficit cnim dum injurüs vexatur vi- — sunt (Isai, 1. 45). Auamien . his . sceleratis homicidis,
dua, ut introeat, genu [lectat, acerbe ingemiscat, — pudore suffusis, ignominia Botatis se reconciliatum
lacrymas fundat, et omnes insidias eorum, a quibus — iri pollicetur, si affectis injuria viduis auxilium feraut.
vexatur, repellat. Ejusmodi enim arma sunt vidu:e, — Postquam enim dixit, Averigm oculos meos, ci πο .
Ίκογσπία, gemitus, ac preces assiduz : perlimce non — exaudiam, inquit : Judicate pupillo, et. justificote vi-

(a) Collata cum Nss. Reg. 1975, et Colberünis 970 et duam, οἳ venite, εἰ disputemus ; εἰ &i [merint. peccata
1650. vestra quasi pherniceun, sicut. nivem dealbabo (1b. 1.
