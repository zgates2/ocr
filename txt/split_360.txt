t6; S. JOANNIS CIRYSOSTOVMI ARCHIEP. CONSTANTINOP, 568
Non est opus «valentibus. medico , sed male habentibus — etiam Filinm suum qui mortüus erat suscitavit. Visne
(Matth. 9. {3). Malo habebat publicanus; propterea — alterius etiam rei symbolum fuisse sterilitatem co-
cito manum illi porrexit : lsaac autem robustior erat; — gnoscere ? Multitudinem fidelium paritura erat Eccle-
propterea reliquit ipsum, ut patientiam ejus augeret. — sia : ut igitur credere minime recusores quo pacto
Sed hzc quidem ex abundanti a nobis sint dicta. Cur — infecumda, infrugifera, sterili& peperisset, pr:ccessit
autem uxor sterilis esset, operz pretium fuerit enar- — €a, quie natura sierilis erat : quie. sterili voluntati
rare : ul, ΦΠ virginem videris factam esse matrem, — viam proemuniret, et Sarra . facta est Ecclesize figura.
€redere non recuses : ut, cum dixerit tibi Judrus, — Να ut illa cum sterilis esset, peperit in senecta : sic
Quo pacto peperit Maria ? dicas illi, Quo pacto pepe- — et ista cum sterilis essel, novissimis temporibus pe-
rit Sarra, et Rebecca, et Rachel? Cum enim mirabila — perit. Atque hoc verum esse, quo ραεῖο Paulus teste-
quidpiam magnumque fuerit eventurum, multe prz- — tur audi : Nos autem sumus libere βΠ (Gal. &. S1).
currunt. figurze. Et quemadmodum cum Imperator ia- — Cum enim figura sit Ecclesiz Sarra, α libera est,
gnedilur,.þræcurrulut milites, πο subito ab imparatis — idcirco addidit, πος liber» filios esse. Et rursus :
excipiatur : ita eum insolitum futurum esset miraculum, — Itaque, fratres, secundum Isaac promissionis filii sumus
przecedunt figurz, ut πος prius exercitati ac dispositi — (Cal. 4. 28). Quid est hoc, Promissionis ? Quem-
uon subito attoniti hzereamus, nec rei preter exspec- — admodum illum patura non peperit, sic neque πος
falionem visz lerriti novitate obstupescamus. Hoc et — nalura peperit, sed gratia Dei. Et rursus : Que autem
in morte. Pr:ecessit Jonas, οἱ mentem nestram exer- — sursum est Jerusalem . libera est, qua esi maler nostra
cuit. Ut enim illum post tres dies cetus evomuit, cum — (Gal. 4. 26), hzc est autem — Ecclesia. Accessistis
in eo proprium et-conveniens non reperisset alimen- — eutm ad Sion montem, inquit, et civitatem Dei viventis,
tum : proprium-quippe.conveniensque mortis alimen- — Jerusalem celestem, et Ecclesiam. primitivorum (Hebr.
um est peccati natura : inde nata est, inde corro- — 12. 22). Si ergo superna Jerusalem est Ecclesia, por-
bórata est, inde etiam alitur. Ut igitur in nobis, si — ΤΟ Jerusalem supernz figura est Sarra, prout dixit,
quando Japidem deglutivimus imprudentes, tum qui- — Όκ sunt, una quidem in. servitutem generans, quee est
dem primum illum vis stomachi concoquere aggredi- — Agar : qug autem sursum est Jerusalem libera est, que
(Γ ; cum vero cibum minime sibi proprium compe- — est mater nostra (Gal. 4. 24-26) : manifestum est Je-
rerit, ubi diutius vii suam concoquendi illi appliouit, — rusalem, qu:e sursum est, figuram esse Sarram ha-
non illum corrumpit, sed virtutem suam perdit:unde — bita ratione partus ac sterilitatis.
nec priorem cibum potest continere, sed defessa cum 5. Scio subtiliora esse qu:e diximus : sed si auen-
ipso illum quoque non sine gravi dolore evomit:ita —damus, nihil eorum nes effugiet, qux dicuntur. Hi
quoque in morte accidit. Lapidem angularem deglut- — ergo pleuiores mysterierum sunt. dogmatumque ser-
vit, neque concoquere ipsum potuit; omuis ejus virtus — moncs : sed si lubet accommodatiorem ad mores cum — .
-languit : propterea cum ipso reliquum quem habe- — ἀς proferemus. Sterilis erat uxor, ut castitatem ma-
bat cibum ejecit , dum hominum naturam simul evo- — rili cognoscas : quod nec illam expulerit, tametsi tum
muit. .Neque enim deinceps illam in finem poterit — id lex nulla proliberet, neque alteram ductam prze-
contincre.-Propterea steriles quoque przecesserunt, ut — ter liberam introduxil : quod — tamen faciunt- muki
etiam partus fides fieret; vel potius non modo ul fides — quzrendorum specie liberorum, ut luxuriam suam
φατίης fleret ; sed-ei exacle examinare velimus, ipsius — expleant, et has ejiciunt, illas introducunt : alii quo-
eliam mortis figuram eterilitatem fuisse reperiemus. — que pellices in eas armant, et infinitis pugnis ac rixis
4. Sed autendite : subule namque est quod dice- — domos complent. At non ita justus ἀ]ο : sed quietus
μς : quippe dicturi sumus quo paeto ad fidem re- — erat dala sibi a Deo uxore conteutus, ac naturae Do-
*urreciionis nos wteri fBarra sterilitas quasi manu — minum precabatur, ul naturz vincula solveret, neque
᾽ ducat. Quonam igitur pacto manu dueit? Quemadmo- — uxori exprobravit. Unde vero constat eum πο expro-
dur illa cum esset mortua beneficio Dei suscitata est, — brasse? Ex ipsa Seriptura. Si exprobrasset, lioc quo-
et lsaaci corpus vivum germinavit : sic cum esset — que Seriptura dixisset, neque tacuisset : siquidem et
mortuus Christus, propria virtute resurrexit. Neque — proclora facinora, et vitia justorum commemorat, ut
. violentam esse nostram expositionem, audi qua ra- — lixc fugiamus, illa zemulemur. Cum igitur apud ilium
tione Paulus testetur. Cum enim de Abrahamo dixis- —ejus nurus ipsius Rachel lamentaretur, et ille objur-
set : Non consideravit emortuam sulvam Sarre, sed — garet, expressitutrumque, nec celavit Scriptura. Cum
-«onforiatus est fide dans gloriam Deo, plenissime sciens, — enim dixisset : Da mihi liberos : si autem non, moriar :
-quis quecumque promisit, potens est et facere.(Rom. 4.— quid ille ait? Numquid Deus ego, qui privavit te fructu
19-21) :-hoc-est, efficere α ex corporibus mortuis — ventris? Da mihi liberos. (Gen. 30. 1. 4]. Muliebris
filius vivus enascatur.: deinde ab illa (ide, utad hanc — postulatio, et a ratione aliena. Marito dicis, α miái
.nos deduceret, adjecit : Non.est scriptum tantum pro- — liberos, et naturze Dominum protermittis ? Idcirco et
pter ipsum, quia repulatum ε! illi.: sed et.propter nos — ille cum illam increpando respondissel, absurdam ejus
(Rom. 4.95. 24). Ad quid! Quibus repulabitur, in» — potitionem repressit, et a quo pelendum esset edocuit.
-quit, credentibus in eum, qui suscitavit Jesum Domi- — At non ita iste, neque tale quidquam ipse dixit, ne-
wum nostrum a mortuis. Quod autem dicit, est ejus- — que apud istum illa conquesta est aut lamentata. .
*modi. Isaacum ex mortuis corporibus excitavit : sie Quid pradicta πος doceant. À weneficiia atque prassti-
