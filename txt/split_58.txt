65 S. JOANNIS CIIRYSOSTOMI ARCHIEP. CONSTANTINOP. 61
€um hujus vitze bona, tum illa qux πος in ezlo in»« — dunt, gaudeamus, benignoque Deo gratias agamus, ut
nent, omnia consequeris. Exemplum habes in Jobo — et przesentem vitam cum facilil.ale. traducannus: e fa-
eertissimum et in apostolis, qui cum propter Deum — turis bonis potiamur, gratia et bemgniulo-bomnn Ηρ-
hujus zvi mala contempsissent, zlerna bona Sunt a$- — siri JesuChristi, cui gloria, honor et itmperium gemper,
sequuti, Pareamus igitur, ot in omnibus, quie acCi- — nuncet in perpetuuni, et in szcula seculorum. Amen.

—— —— ..'...ὶ.ἈὨἈἩἩἳὶἈ}Ἡ)Ἀἱὐουο.ρχγολἃυρρροαωατετωΧ Ἅµγὓειἲ]ὶὲὶὲἛ.νὶιἩ-επ

MONITUM

llasce conciones, longo intervallo in prius Editis separatas, ut continenter ponamus, et series ipsa tem-
poris, et argumenti ratio postulat. Cum seriem temporis dico, ne ρυίες me ita tempus asseculum esse,
ut annum vel cerio statuere, vel probabilibus admodum conjecturis indicare possim ; sed de serie et
ordine temporis loquor, quem inter se habent Homiliz: ἀς Actorum inscriptione, ita ut assignare in
promplu sit, quze sit prima, φ secunda, ete. ld vero clare docet ipse Clirysostomus infra in ea, quam
tertiam ordine ponimus. Ibi vero num. 2 legitur : Εο fit, ut cum ad quartum jam diem expositionem pro-
duxerimus, uecdum tamen unam inscriptionem poluerimus praterire, sed adhwc circa illam versemur. Et
paulo post: Jiaque primo die non esse lemere pretereunda» inecriptiones. dicebam, quo. tempore titulum
vobis Altaris legi, ac Pauli sapientiam ostendi..... 1n μος desiit priori die tota doctrina : post illam secundo
die, qui libri esset illius auctor quasivimus, et invenimus Dei gratia. Lucam cvangelistam : multisque vobis
demonstrationibus rem in quastione positam probavimus. Demum clarius paucis interpositis : Prima igitur
die, ες inscriptione ; secunda vero die, de eo lyui librum scripsit; tertia die disseruimus apud eos, qui advene-
vant, de initio Scripture : εἰ ostendimus quid sit aclum, quid . sit. miraculum, et quid sil. conversatio, quid
signum et prodigium ας virtus.., Hodie reliquum inscriptionis opera pretium est dicamus, et quid significel

Apostoli nomen ostendamus. Prima igitur homilia ea est, in qua non Lemere prztereundas esse sacrorum

Librorum inscripliones docet, et de inscriptione Altaris, nimirum, 1GNOTO DEO, agit. lllam vero Chry-

sostomo duce primam locamus. Secundam ubi de auctore libri Actorum edisserebat, et Lucam esse de-

monstrabat, reperimus quidem ; sed, heu ! misere deformatam, et cum spuriis immixtam. Est nempe

Momilia inscripta, περὶ ᾿Αναλῆψεως, de Assumptione sive Ascensione Domini, et Chrysostomo ascripta :

ubi qu:e prima feruntur, Chrysostomo nostro indigna videntur esse. Sub bac adjiciuntur alia, 4σ cum

prioribus non cohzrere videntur, etlicet. melioris notz sint, utrum Chrysostomo sint aseribenda non
ausim aflirmare. Sequitur postea pars illa, ubi auibusdam pramissis circa. inscriptionem libri Actorum,
υς ad prasens institutum apprime quadrant, de hujus auctore egregie disseritur, et Lucam esse proba-
tur. Hic vero Chrysostomuni agnosco ; nec dubito quin tam ea ανὰ de inscriptione Áciorum, quam ea
quz de hujus libri auctore feruntur, ad eam homiliam pertincant, quam ut secundam a se dictam hie
niemorat Clirysostomus. Hinc quzritur, quoties a Resurrectione ad Ascensionem usque Dominus Cliristus
apparuerit ; hanc quoque partem Chrysostomi esse puto: reliqua vero omnia, qu:x in hae longissima
homilia babentur, α inepta at Clirysostomo indigna sunt. ltaque male auctam οἱ consarcinatam homi-
liam, etsi quaedam γνῆσια el ad prasens argumentum perlinentia hbabeat, cum sinceris admiscere non
ausi sumus ; sed ad finem hujus tomi ablegavimus. Tertia quam, allera ad calcem rejecta, secundam po-
nimus, ea est in qua de inscriptione Actorum agitur, nec non de differentia inter miraculuni et actun, ete.
Quarla, quam supra memorata de causa terliam inscribimus, εα est qua de utilitate lectionis Scriptura-
rum agit, et ut tituli Áctuum. Apostolorum postremam vocem explicet, perquirit qnid signilicet Apostoli
nomen, Huic quartam suljungimus eam in qua quarit cur Ácta Apostolorum in Pentecoste legantur :
qui sese prodit aliis subnectendam esse : etenim in ea ia legitur : Dixi tum . temporis, a quo . scriptus
fseril liber Áclorum, et quis operis istius auctor fuerit; imo vero non quis auctor operis, sed quis minister.

on enim ille qua dicta sunt produxil, sed iis,Juw dicta sunt, ministravit. Dixi de Actis ipsis, et quid tam-
dem sibi velit nomen il'ud Actorum : dixi etiam de Apostolorum appellutione : jum necesse est. dicamus, qua
de camaa statuerint patres nostri, κὲ liber Actorum in Pentecoste legeretur.

ltaque luce clarius est has continue, modico inter singulas interposito tempore, habitas fuisse: ct
quidem non diu post Paschalem solemnitatein, ut ex iis, ἀν ἀθ nuper illuminatis circa linem primz et
terüize honmilie dicuntur, arguitur. Pratter Πς vero Paschatis nuper elapsi notam, alteram eruimus ex
his verbis homiliz l, num. ξ : Non sunt igitur malum divitie, sed i.legitimus earum usus est malum : et
sicut nuper de ebrielate verba faciens, non vinum accusabam, quippe cum omnis creatura Dei bona sit, que
cum gratiarum actione percipitur ; εά malum. usum, etc.. Hic perro unnotare prorsus videlur Bomiliam
contra Ebriosos et de Hesurrectione, quam T. 2 edidimus : ubl contra temulentos multa disserens, hac
inter alia dicit : Abstineamus ab ebrietate : non dico, Abstineamus a viuo, sed, Ab ebrietate abstincamus.
Nom vinum e[fcil ebrietatem : est enim Dei creatura : Βεῖ porro creatura uihil efficit mali, sed voluntas ma-
ligna v[ficit ebrietatem, etc. Ex his porro verbis arguitur homiliam illam, qux ut nuper babita memoratur
in hac prima concione in Áctorum initium, ean igsam esse, qua contra Ebriosos in die Kesurrectionis
dicta fuit, atque adeo hanc primam paucis post Pascha diebus habitam [uisse, ldipsum vero probatur
etiam -χ alio hujus homiliz loco, num. 3, ubi dicitur : Superioribus apud υος diebus de apostolicis ver-
bis, deque evangelicis disseruimus, cum de Juda verba (aceremus : disseruimns et de. prophelicis, hodierno
die volumus de ABoslolorun: Actis, οἷο, Hic omnino videtur agi de llomilia in Judam, qua habita fuit
feris V in Cona Domini, ubi de apostolicis et de propheticis verbis edisserit. Hujus porro liowiiliz dupli-
cem texiuni edidimus, quia cum priorem de Juda, cujus initium cst, Ὀλίγα ἀνάγκη σήμερον, aliquol ante — ,
üunis habuisset, posteriorem, qux sic init, Ἐθονλόμην, ἀγαπητοὶ, τῆς κατὰ τὸν πατριάρχην, camdem, quaum
