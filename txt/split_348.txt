265 $. JOANNJS CHAYSOSTOMI ARCUIEP. CONSTANTINOP. 3556
linguis prophetarum estcompositum. Quam ob cqu- — se neque hoe sunt, ncque illed : sed o ratione
&um perpetuum est, οἳ neque temporum longioquitatle — propositi ae. voluntatis eorum qui utuntur . illig , aut
debilitatur, neque morborum virtute superatur. Nam — hoe aut. illud. fiunt, Nam si ad humanitaten quidere
ujedicorum quidem medicamenta utcoque defectu αο - divitiis utare, tibi res ad occasioneta boni tradacetur z
vitio laboran! ; duin receulia sunt, vum suam produnt: — Sip auterm ad rapinas, avaritiam et injuriam , in cous
cam vero multum teuporis est elapsum, in. modum — (rarium ipsius usum convertes : xerumtamen nou
corporum seneciute confectorum imbeciliora red- — suntin causa divitiz, sed is qui ad injuriaim divitiis
duntur ; quin. etiam perszpe morborum difliculiate — est usus. lia quoque de paupertate dici potest : si-
vincuntur, quippe α liumana sint : at divinum me- — quidem illam patieuter tuleris Domino gratias agene,
dicamentum tale non est, sed inulto tempore iuter- — res tibi occasio fiet ac materia coronarum : sin autera
jecto suam omnem vim retinet. Sane ex eo tempore, — idcirco blasphemus fueris in Creatorem, ejusque pro-
quo visit Moyses (ab illo enim initium est Scriptu- — videntiam aecusaveris , rem ad malum usun rursus
rarum), tam mullos lomines sauavit, οἱ suam virta-.— traduxisti. Sed quemadiodum illic avaritize fraudisve
temn non. amisit : θθ neque ab ulla umquam :egritm- — diviti:t causa non sunt , sed ἰς qui imnale divitiis uti-
dine superatum est. Πος medicainentum non nume- — tur : sic etiam blasphemi:e culpam non rejiciemus in
rata pecunia licet aceipere, sed qui sinceram τοῖση- — paupertatem,, sed in eum qui rem moderate ferre
tatem et affectum. exhibet, totum illud seeum repor- — noluit. Übique namque eum lsus, tum vituperium ex
tat. ldcirco divites simul et pauperes pariter bac inec- — semtentia οἳ voluntate nostra pendet. Bomo sunt di-
dicina fruuntur. Nam ubi quidem pecunias impendi — vitizs , sed non simpliciter, verum iHi, eui peceatuns
necesse est, qui loeuples est, utilitatis iL particeps : — non θ68 : el rursus inala est paupertas , sed men sim-
pauper auteg Sxpe lucri expers disceedit ; cum tanti — yliciler, verum in ore impii, quouiam ρς fett, quo-
reditus illi non suppetant, ut ad medicamenium cen- — niam blasphemat, queniam Creatorem accusat.
ficiendum sufliciant. Hie vero quoniam pecuniam nu- 9. INec divitie nce pauperias vituperandg.—Ne igitur
tucrare non permitüiur, sed fideset voluutas est ex- — divitias aceusemus , neque paupertatem simpliciter
hibeuda, qui numerare potest ista et cum akicritate — vituperemus, sed εος qui hís rebüs recte uti nolunt :
persolvere, hic potissimum percipit utilitatem : quah-— res enim ipex sunt in medio positz. Sed quod dice-
doquidem lec sunt quze pre mercede medicin:e i — bam. ( bonum enim est ut ad prius illud argumentun
tius exiguntur. Et dives et pauper pariter hanc utili-.— pevertamur ), et dives οἳ pauper eadem eum fiducia
latem participant : jmo vero non pariter uiilifatetn — et libertate ς nostris medicamentis freuntur : $x-
partieipant, sed imajori percepta pauper ab:cedit. — penumero ctiaw majori cum studio pauper. Non ας
Quid ita? Quod nimirum dives variis prasoecupatus — medicamentorum istorum pr:ecipua laus est, quod
sollicitudinibus , superbia tumens , etopibus inflalU& — amimas sanent, quod temporis longimjeitate non
ac fastu, desidias deditus ac negligentize, nou adnio- — corrumpantur, quod a morbis non vincantur , quod
duin atiente, neque magno cum studio tnedicinamaU- — eorum, utilitas gratis proposita sit, quod ex xquo di-
ditionis Scripturz rceipiat ; pauper.autem a deliciis — vitibus e& pouperibus pateat medicina : sed ct aliud
et ingluvie, ac negligentia immunis, multamque iude — quidpiam nihilo minus bis bonis habent. Quodnam
animz sux concilians pbilosophiam, dum totum tem- — jilud taudem cst? Quod οος qui ad hanc medici ofti-
pus iw opere manuum. ac legitimis laboribus consU- — cinam veniunt non divulgamus. Nam illi quidem qui
mit, attentior ac robustior evadat, ac majori cum di- — ad profanas medicorum officinas abeunt , multos ba-
ligentia qu:e dicuntur pereipiat ; quo fit, ut majori — bent. vulnerum spectatores : ac nisi prius medicus
pretio persoluto, majori decerpta utilitate discedat. ulcus detegat , mediramentum non adhibet : Μο vero
$. Mec a me non cv dicuntur consilio , ul divileS — non ita fi , Sed cum innumeros videamus zegrotos ,
quoscunique vitaiperem, nec ut pauperes quoscumque — oceutte illós curamus. Neque enim in medium addu-
Jaudem : nam neque divitie malum sunt, sed divitiis — ctis neceatoribus, eorum deinde peccata divulgaimus :
ale uti ; neque paupertas. bonum , sed paupertate — sed communi omnibus doctrina proposita, conscientia:
bene uti. Torquebatur dives ille qui ztate Lazari — reliuquimus auditorum, ut convenientem suo vulneri
vixerat, non quia dives fuerat, sed quia crudelis — ex iis quae dicta sunt eliciant medicinam. Prodit enigy
fuerat eà inhasianus. Laudabatur pauper ille in sinu — al oratoris lingua doctrinze sermo qui vituperatiomem
Abrahz, non quia pauper fuerat,sed quia pauper- — vitii conlinet, laudcm virtutis, reprehensionem Iuxu-
ialem cum gratiarum actione toleraverat. Res enim — rie, commendationem castitatis, aecusalionem su-
alie { attendite. diligenter ο εά qux dicimus : potg- — perbiz, przconium modestiz, tamquam varium mwl-
runt enim sufficientem philosophiam modesüanique — tiplexque. pharmacum ex omnibus speciebns compo-
voliis inserere , ac vitiosam omnem cogitationem-ex- — situm : potro conveniens sibi et utile ut accipiat, eat
pellere, atque eflicere ut recium de rebus judicium — uniuscujusque auditorum. Procedit igitur aperte
(vratis ) ; res igitur alize sunt natura bonze, alie plaue — sermo , ει cujusque conscienli:e insidens , latenter
coutrarix ; alie qu:e nec bonze sunt nec male, sed — suam exlhibet medicinam, et prius quam xgritudo
medium quemdam locum tenent. Bonum est quidUam — publicetur, &rpe restituit sanitatem.
natura $ua pietas , nialum  impielas : bonum virtus, 4. Heri quidem οογίο audiistis quo paeto virtetem
walum improbitas : porro divitiae ac paupertas per — orationis faudarim , quo pacto cos qui ucgli,cn:ec
