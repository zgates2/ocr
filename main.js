const Tesseract = require("tesseract.js");
const worker = Tesseract.createWorker();
const fs = require("fs");

async function main(){
  const files = fs.readdirSync("./tif");
  await worker.load();
  await worker.loadLanguage("lat+ell");
  await worker.initialize("lat+ell");
  for (const file of files) {
    console.log("Running:", file);
    const tif = fs.readFileSync(`./tif/${file}`);
    const {data : {text}} = await worker.recognize(tif);
    fs.writeFileSync(`./txt/${file.split(".")[0]}.txt`, Buffer.from(text));
  }
  await worker.terminate();
}

main();
