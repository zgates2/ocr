#!/bin/bash
set +x

for file in $(ls ./pdf);do
    echo "Running $file"
    gs -dNOPAUSE -q -r300x300 -sDEVICE=tiffg4 -dBATCH -dNOSAFER -sOUTPUTFILE=./tif/${file}.tif ./pdf/${file} &
done